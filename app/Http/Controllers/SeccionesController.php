<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Services\GuzzleHttpRequest;

class SeccionesController extends Controller
{
    protected $client;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GuzzleHttpRequest $client)
    {
        $this->middleware('auth');
        $this->client = $client;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
      switch ($id) {
        case "organigrama":
            return view('layouts.dashboards.panels.secciones.municipio');

        case "presupuesto":
            $municipio_2016 = $this->client->get('PRESU-Y-GASTA-POR-SECRE/data.ajson/', [
                'query' => [
                    'auth_key' => env('DATOS_API_KEY'),
                    'function0' => 'SUM[column3]',
                    'function1' => 'SUM[column4]'
                ]
            ]);
            //dd($municipio->result[0]);
            $secretarias_2016 = $this->client->get('PRESU-Y-GASTA-POR-SECRE/data.ajson/', [
                'query' => [
                    'auth_key' => env('DATOS_API_KEY'),
                    'groupBy0' => 'column1',
                    'function0' => 'SUM[column3]',
                    'function1' => 'SUM[column4]'
                ]
            ]);

            $municipio_2017 = $this->client->get('PRESU-Y-GASTA-POR-SECRE/data.ajson/', [
                'query' => [
                    'auth_key' => env('DATOS_API_KEY'),
                    'pArgument0' => '2017',
                    'function0' => 'SUM[column3]',
                    'function1' => 'SUM[column4]'
                ]
            ]);

            $secretarias_2017 = $this->client->get('PRESU-Y-GASTA-POR-SECRE/data.ajson/', [
                'query' => [
                    'auth_key' => env('DATOS_API_KEY'),
                    'pArgument0' => '2017',
                    'groupBy0' => 'column1',
                    'function0' => 'SUM[column3]',
                    'function1' => 'SUM[column4]'
                ]
            ]);

            return view('layouts.dashboards.panels.secciones.presupuesto', [
                'municipio_2016' => json_encode($municipio_2016->result[0]),
                'secretarias_2016' => json_encode($secretarias_2016->result),
                'municipio_2017' => json_encode($municipio_2017->result[0]),
                'secretarias_2017' => json_encode($secretarias_2017->result),
                'modified_at' => json_encode($municipio_2016->modified_at),
                'guid' => json_encode($municipio_2016->guid),
                'category' => json_encode($municipio_2016->category_name)
            ]);

        case "compras-y-proveedores":
            $compras_jurisdiccion_2012 = $this->client->get('COMPR-POR-JURIS/data.ajson/', [
                'query' => [
                    'auth_key' => env('DATOS_API_KEY')
                ]
            ]);

            $compras_publicidad_2016 = $this->client->get('COMPR-DE-PUBLI-ANO-2016/data.ajson/', [
                'query' => [
                    'auth_key' => env('DATOS_API_KEY')
                ]
            ]);

            $compras_publicidad_2016_total = $this->client->get('COMPR-DE-PUBLI-ANO-2016/data.ajson/', [
                'query' => [
                    'auth_key' => env('DATOS_API_KEY'),
                    'function0' => 'SUM[column1]'
                ]
            ]);

            return view('layouts.dashboards.panels.secciones.compras-y-proveedores', [
                'jurisdiccion' => json_encode($compras_jurisdiccion_2012->result),
                'publicidad' => json_encode($compras_publicidad_2016->result),
                'publicidad_total' => json_encode(array_values($compras_publicidad_2016_total->result[0])[0])
            ]);

        case "politicas-sociales":
            $programas_2016_dependencias = $this->client->get('PROGR-SOCIA-POR-MONTO/data.ajson/', [
                'query' => [
                    'auth_key' => env('DATOS_API_KEY'),
                    'limit' => '1'
                ]
            ]);

            $programas_2016_total = $this->client->get('PROGR-SOCIA-POR-MONTO/data.ajson/', [
                'query' => [
                    'auth_key' => env('DATOS_API_KEY'),
                    'filter0' => 'column0[>=]2016-01-01',
                    'filter1' => 'column0[<=]2016-12-31',
                    'where' => '(filter0%20and%20filter1)',
                    'function0' => 'SUM[column1]',
                    'function1' => 'SUM[column2]',
                    'function2' => 'SUM[column3]',
                    'function3' => 'SUM[column4]',
                    'function4' => 'SUM[column5]',
                    'function5' => 'SUM[column6]',
                    'function6' => 'SUM[column7]',
                    'function7' => 'SUM[column8]',
                    'function8' => 'SUM[column9]',
                    'function9' => 'SUM[column10]'
                ]
            ]);

            return view('layouts.dashboards.panels.secciones.politicas-sociales', [
              'programas_dependencias' => json_encode($programas_2016_dependencias->result[0]),
              'programas_total' => json_encode($programas_2016_total->result[0])
            ]);

        case "salud-y-medioambiente":
            $residuos_2014_total = $this->client->get('PROGR-DE-GESTI-DE-RESID/data.ajson/', [
                'query' => [
                    'auth_key' => env('DATOS_API_KEY'),
                    'filter0' => 'column1[==]2014',
                    'filter1' => 'column0[contains]Total',
                    'where' => '(filter0%20and%20filter1)'
                ]
            ]);

            return view('layouts.dashboards.panels.secciones.salud-y-medioambiente', [
              'residuos_mes' => json_encode($residuos_2014_total->result)
            ]);

        case "seguridad-social":
            return view('layouts.dashboards.panels.secciones.seguridad-social');
      }
    }
}

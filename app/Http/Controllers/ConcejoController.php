<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Services\GuzzleHttpRequest;

class ConcejoController extends Controller
{
    protected $client;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GuzzleHttpRequest $client)
    {
        $this->middleware('auth');
        $this->client = $client;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // descomentar despues
      // $municipio = $this->client->get('PRESU-Y-GASTA-POR-SECRE/data.ajson/', [
      //     'query' => [
      //         'auth_key' => env('DATOS_API_KEY'),
      //         'function0' => 'SUM[column3]',
      //         'function1' => 'SUM[column4]'
      //     ]
      // ]);

      // $municipio = json_decode($municipio->getBody()->getContents());

      // $secretarias = $this->client->get('PRESU-Y-GASTA-POR-SECRE/data.ajson/', [
      //     'query' => [
      //         'auth_key' => env('DATOS_API_KEY'),
      //         'groupBy0' => 'column1',
      //         'function0' => 'SUM[column3]',
      //         'function1' => 'SUM[column4]'
      //     ]
      // ]);

      // $secretarias = json_decode($secretarias->getBody()->getContents());

      // dd($municipio, $secretarias);

      // $time = strtotime($aux->modified_at);
      // $newformat = date('Y-m-d',$time);
      // echo $newformat;
      // 2003-10-16

      //return view('home');

      // dd([1,2,3,4,5,6]);
      return view('layouts.dashboards.panels.concejo');
      // [
      //     'municipio' => json_encode($municipio->result[0]),
      //     'secretarias' => json_encode($secretarias->result),
      //     'description' => json_encode($municipio->description),
      //     'title' => json_encode($municipio->title),
      //     'modified_at' => json_encode($municipio->modified_at),
      //     'guid' => json_encode($municipio->guid),
      //     'category' => json_encode($municipio->category_name)
      // ]

      // $datos = [1,2,3,4,5,6];
      // return view('home')->with('datos', json_encode($datos));
    }
}

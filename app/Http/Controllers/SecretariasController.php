<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Services\GuzzleHttpRequest;

class SecretariasController extends Controller
{
    protected $client;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(GuzzleHttpRequest $client)
    {
        $this->middleware('auth');
        $this->client = $client;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
      switch ($id) {
        case "asesoria-letrada":
            return view('layouts.dashboards.panels.secretarias.asesoria-letrada');
        case "gestion-ambiental":
            return view('layouts.dashboards.panels.secretarias.gestion-ambiental');
        case "gobierno":
            return view('layouts.dashboards.panels.secretarias.gobierno');
        case "hacienda":
            return view('layouts.dashboards.panels.secretarias.hacienda');
        case "infraestructura":
            return view('layouts.dashboards.panels.secretarias.infraestructura');
        case "innovacion":
            return view('layouts.dashboards.panels.secretarias.innovacion');
        case "modernizacion":
            return view('layouts.dashboards.panels.secretarias.modernizacion');
        case "politicas-sociales":
            return view('layouts.dashboards.panels.secretarias.politicas-sociales');
        case "salud":
            return view('layouts.dashboards.panels.secretarias.salud');
        case "seguridad":
            return view('layouts.dashboards.panels.secretarias.seguridad');
        case "cultura":
            return view('layouts.dashboards.panels.secretarias.cultura');
      }
    }
}

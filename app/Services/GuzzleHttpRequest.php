<?php

namespace App\Services;
use GuzzleHttp\Client;

class GuzzleHttpRequest
{
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function get($url, $params)
    {
        $response = $this->client->request('GET', $url, $params);

        return json_decode($response->getBody()->getContents());
    }
}

@extends('welcome')

@section('content')
<div class="content">
    <div class="title dark-bg">
        <strong>Bahía Blanca</strong>
    </div>

    <div class="links m-t-20">
        <a href="http://www.bahia.gob.ar" target="_blank">Municipio</a>
        <a href="http://datos.bahiablanca.gob.ar/home" target="_blank">Datos abiertos</a>
        <a href="http://www.bahia.gob.ar/transparencia/" target="_blank">Transparencia</a>
        <a href="https://www.uns.edu.ar/" target="_blank">Universidad</a>
        <a href="https://cs.uns.edu.ar/home/" target="_blank">DCIC</a>
        <a href="https://lissi.cs.uns.edu.ar/" target="_blank">LISSI</a>
        <a href="http://www.cic.gba.gob.ar/" target="_blank">CIC</a>
    </div>
</div>
@endsection

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Posts</title>
        <link rel="stylesheet" href="/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2>{{ $post->title }}</h2>
                </div>
                <div class="panel-body">
                    {{ $post->body }}
                </div>
                <br>
                <a href="/">Regresar</a>
            </div>
        </div>
    </body>
</html>

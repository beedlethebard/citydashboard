<!-- Scripts -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/adminlte.min.js') }}"></script>
<script type="text/javascript">

    if (Boolean(sessionStorage.getItem("sidebar-toggle-collapsed")))
    {
        $("body").removeClass('sidebar-collapse')
    }

    if (Boolean(sessionStorage.getItem("treeview-menu-open")))
    {
        $("#toggle-secretarias").addClass('menu-open')
    }

    $('.sidebar-toggle').click(function()
    {
        event.preventDefault();

        if (Boolean(sessionStorage.getItem("sidebar-toggle-collapsed")))
        {
            sessionStorage.setItem("sidebar-toggle-collapsed", "");
        }
        else
        {
            sessionStorage.setItem("sidebar-toggle-collapsed", "1");
        }
    })

    $('#toggle-secretarias').click(function()
    {
        if (Boolean(sessionStorage.getItem("treeview-menu-open")))
        {
            sessionStorage.setItem("treeview-menu-open", "");
        }
        else
        {
            sessionStorage.setItem("treeview-menu-open", "1");
        }
    })
</script>

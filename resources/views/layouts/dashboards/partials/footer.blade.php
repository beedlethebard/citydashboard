<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <a href="https://www.uns.edu.ar/" target="blank"><b>UNS</b></a> -
        <a href="https://cs.uns.edu.ar/" target="blank"><b>DCIC</b></a> -
        <a href="https://lissi.cs.uns.edu.ar/" target="blank"><b>LISSI</b></a>
    </div>
    <!-- Default to the left -->
    <strong>Diseño de Tableros de Control en el sector público</strong>
</footer>

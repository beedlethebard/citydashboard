<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="{{ Request::is('home') ? 'active' : '' }}">
        <a href="{{ route('home') }}">
          <i class="fa fa-home fa-fw fa-sidebar"></i> <span>@lang('custom.home')</span>
        </a>
      </li>
      <li class="header text-uppercase">@lang('custom.dependencies')</li>
      <li class="{{ Request::is('intendencia') ? 'active' : '' }}">
        <a href="{{ route('intendencia') }}">
          <i class="fa fa-group fa-fw fa-sidebar"></i> <span>Intendencia</span>
        </a>
      </li>
      <li class="{{ Request::is('concejo') ? 'active' : '' }}">
        <a href="{{ route('concejo') }}">
          <i class="fa fa-comments fa-fw fa-sidebar"></i> <span>Honorable Concejo Deliberante</span>
        </a>
      </li>
      <li id="toggle-secretarias" class="treeview {{ Request::is('secretarias/*') ? 'active' : '' }}">
          <a href="#">
            <i class="fa fa-pie-chart fa-fw fa-sidebar"></i>
            <span>@lang('custom.dependencies')</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ Request::is('*/asesoria-letrada') ? 'active' : '' }}">
              <a href="{{ route('secretarias', ['id' => 'asesoria-letrada']) }}">
                <i class="fa fa-circle fa-fw fa-sidebar asesoria-letrada"></i> <span>Asesoría Letrada</span>
              </a>
            </li>
            <li class="{{ Request::is('*/gestion-ambiental') ? 'active' : '' }}">
              <a href="{{ route('secretarias', ['id' => 'gestion-ambiental']) }}">
                <i class="fa fa-circle fa-fw fa-sidebar gestion-ambiental"></i> <span>Gestión Ambiental</span>
              </a>
            </li>
            <li class="{{ Request::is('*/gobierno') ? 'active' : '' }}">
              <a href="{{ route('secretarias', ['id' => 'gobierno']) }}">
                <i class="fa fa-circle fa-fw fa-sidebar gobierno"></i> <span>Gobierno</span>
              </a>
            </li>
            <li class="{{ Request::is('*/hacienda') ? 'active' : '' }}">
              <a href="{{ route('secretarias', ['id' => 'hacienda']) }}">
                <i class="fa fa-circle fa-fw fa-sidebar hacienda"></i> <span>Hacienda y Desarrollo Económico</span>
              </a>
            </li>
            <li class="{{ Request::is('*/infraestructura') ? 'active' : '' }}">
              <a href="{{ route('secretarias', ['id' => 'infraestructura']) }}">
                <i class="fa fa-circle fa-fw fa-sidebar infraestructura"></i> <span>Infraestructura</span>
              </a>
            </li>
            <li class="{{ Request::is('*/innovacion') ? 'active' : '' }}">
              <a href="{{ route('secretarias', ['id' => 'innovacion']) }}">
                <i class="fa fa-circle fa-fw fa-sidebar innovacion"></i> <span>Innovación, Tecnología y Desarrollo Creativo</span>
              </a>
            </li>
            <li class="{{ Request::is('*/modernizacion') ? 'active' : '' }}">
              <a href="{{ route('secretarias', ['id' => 'modernizacion']) }}">
                <i class="fa fa-circle fa-fw fa-sidebar modernizacion"></i> <span>Modernización y Calidad de Gestión</span>
              </a>
            </li>
            <li class="{{ Request::is('*/politicas-sociales') ? 'active' : '' }}">
              <a href="{{ route('secretarias', ['id' => 'politicas-sociales']) }}">
                <i class="fa fa-circle fa-fw fa-sidebar politicas-sociales"></i> <span>Políticas Sociales</span>
              </a>
            </li>
            <li class="{{ Request::is('*/salud') ? 'active' : '' }}">
              <a href="{{ route('secretarias', ['id' => 'salud']) }}">
                <i class="fa fa-circle fa-fw fa-sidebar salud"></i> <span>Salud</span>
              </a>
            </li>
            <li class="{{ Request::is('*/seguridad') ? 'active' : '' }}">
              <a href="{{ route('secretarias', ['id' => 'seguridad']) }}">
                <i class="fa fa-circle fa-fw fa-sidebar seguridad"></i> <span>Seguridad y Protección Ciudadana</span>
              </a>
            </li>
            <li class="{{ Request::is('*/cultura') ? 'active' : '' }}">
              <a href="{{ route('secretarias', ['id' => 'cultura']) }}">
                <i class="fa fa-circle fa-fw fa-sidebar cultura"></i> <span>Instituto Cultural</span>
              </a>
            </li>
          </ul>
      </li>
      <li class="header text-uppercase"> @lang('custom.main-sections')</li>
      <li class="{{ Request::is('secciones/organigrama') ? 'active' : '' }}">
        <a href="{{ route('secciones', ['id' => 'organigrama']) }}">
          <i class="fa fa-1 fa-sitemap fa-fw fa-sidebar"></i> <span>@lang('custom.section-1')</span>
        </a>
      </li>
      <li class="{{ Request::is('secciones/presupuesto') ? 'active' : '' }}">
        <a href="{{ route('secciones', ['id' => 'presupuesto']) }}">
          <i class="fa fa-2 fa-money fa-fw fa-sidebar"></i> <span>@lang('custom.section-2')</span>
        </a>
      </li>
      <li class="{{ Request::is('secciones/compras-y-proveedores') ? 'active' : '' }}">
        <a href="{{ route('secciones', ['id' => 'compras-y-proveedores']) }}">
          <i class="fa fa-3 fa-calculator fa-fw fa-sidebar"></i> <span>@lang('custom.section-3')</span>
        </a>
      </li>
      <li class="{{ Request::is('secciones/politicas-sociales') ? 'active' : '' }}">
        <a href="{{ route('secciones', ['id' => 'politicas-sociales']) }}">
          <i class="fa fa-4 fa-life-ring fa-fw fa-sidebar"></i> <span>@lang('custom.section-4')</span>
        </a>
      </li>
      <li class="{{ Request::is('secciones/salud-y-medioambiente') ? 'active' : '' }}">
        <a href="{{ route('secciones', ['id' => 'salud-y-medioambiente']) }}">
          <i class="fa fa-5 fa-heartbeat fa-fw fa-sidebar"></i> <span>@lang('custom.section-5')</span>
        </a>
      </li>
      <li class="{{ Request::is('secciones/seguridad-social') ? 'active' : '' }}">
        <a href="{{ route('secciones', ['id' => 'seguridad-social']) }}">
          <i class="fa fa-6 fa-shield fa-fw fa-sidebar"></i> <span>@lang('custom.section-6')</span>
        </a>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>

@extends('layouts.dashboards.app')

@section('dashboard-header')
    <section class="content-header">
      <h1>
        @lang('custom.section-4')
      </h1>
      <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-dashboard"></i> @lang('custom.home')</a></li>
        <li class="active">@lang('custom.section-4')</li>
      </ol>
    </section>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-8">
      <div class="box box-home box-home-4">
        <div class="box-header with-border">
          <h3 class="box-title">Montos de los programas sociales municipales, por fecha</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                <div class="chart-responsive pd-15">
                  <canvas id="barChart1" height="250"></canvas>
                </div>
            </div>
          </div>
        </div>
        <div class="box-footer">
          <div class="description-block">
            <div class="row">
              <div class="col-md-6 border-right">
                <h5 class="description-percentage text-green">
                  <span>
                    <i class="fa fa-tag"></i>
                  </span> @lang('custom.category') <strong>Políticas Sociales</strong>
                </h5>
                <span> @lang('custom.dataset') <strong>PROGR-SOCIA-POR-MONTO</strong></span><br>
                <span> @lang('custom.last-mod') <strong class="FechaModificacion">3 de Octubre de 2017</strong></span>
              </div>
              <div class="col-md-6">
                <h2>Total: $ 61.106.863</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('footer')

@endsection

@section('data-scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
  <script type="text/javascript">

    var myData = <?php echo json_encode($programas_dependencias); ?>;
    var myData2 = JSON.parse(myData);
    myData2.shift();
    console.log(myData2);

    var programas_dependencias = myData2;
    var programas_total = [];

    myData = <?php echo json_encode($programas_total); ?>;
    myData2 = JSON.parse(myData);
    console.log(myData2);

    var i;
    for (i = 0; i < myData2.length; i++)
    {
        var total = myData2[i]
        programas_total.push(parseFloat(total.replace(/,/g , "")));
    };

    var sum = programas_total.reduce((a, b) => a + b, 0);
    console.log(sum); // 6

    var data = {
          labels: programas_dependencias,
          datasets: [
            {
              label: "Monto",
              backgroundColor: ["#3e95cd", "#c02042", "#555", "#b32666", "#00a43d", "#37967b", "#b2bb38", "#2ea6c3", "#e3007a", "#e43117", "#c01190", "#d7b200", "#2865be"],
              data: programas_total
            }
        ]
    };

    var pieChartCanvas = $('#barChart1').get(0).getContext('2d');

    var pieChart = new Chart(pieChartCanvas, {
        type: 'bar',
        data: data,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true,
                        callback: function (tick, index, ticks) {
                            return numeral(tick).format('0.0a');
                        }
                    }
                }],
                xAxes: [{
                    display: false
                }]
            },
            legend: {
                display: false
            },
            labels: {
                display: false
            },
            maintainAspectRatio: false
        }
    });


  </script>
@endsection

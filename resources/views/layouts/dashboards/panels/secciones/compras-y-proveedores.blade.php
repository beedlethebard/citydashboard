@extends('layouts.dashboards.app')

@section('dashboard-header')
    <section class="content-header">
      <h1>
        @lang('custom.section-3')
      </h1>
      <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-dashboard"></i> @lang('custom.home')</a></li>
        <li class="active">@lang('custom.section-3')</li>
      </ol>
    </section>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-4">
      <div class="box box-home box-home-3">
        <div class="box-header with-border">
          <h3 class="box-title">Compras por jurisdiccion del año 2012</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                <div class="chart-responsive pd-15">
                  <canvas id="barChart1" height="250"></canvas>
                </div>
            </div>
          </div>
        </div>
        <div class="box-footer">
          <div class="description-block">
            <h5 class="description-percentage text-maroon">
              <span>
                <i class="fa fa-tag"></i>
              </span> @lang('custom.category') <strong>Compras</strong>
            </h5>
            <span> @lang('custom.dataset') <strong>COMPR-POR-JURIS</strong></span><br>
            <span> @lang('custom.last-mod') <strong class="FechaModificacion">16 de Mayo de 2016</strong></span>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-8">
      <div class="box box-home box-home-3">
        <div class="box-header with-border">
          <h3 class="box-title">Compras de Publicidad - Año 2016</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                <div class="chart-responsive pd-15">
                  <canvas id="barChart2" height="250"></canvas>
                </div>
            </div>
          </div>
        </div>
        <div class="box-footer">
          <div class="description-block">
            <div class="row">
              <div class="col-md-7 border-right">
                <h5 class="description-percentage text-maroon">
                  <span>
                    <i class="fa fa-tag"></i>
                  </span> @lang('custom.category') <strong>Compras</strong>
                </h5>
                <span> @lang('custom.dataset') <strong>COMPR-DE-PUBLI-ANO-2016</strong></span><br>
                <span> @lang('custom.last-mod') <strong class="FechaModificacion">22 de Marzo de 2017</strong></span>
              </div>
              <div class="col-md-5">
                <h2>Total: $ 27.406.587,58</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('footer')

@endsection

@section('data-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<script type="text/javascript">

    var myData = <?php echo json_encode($jurisdiccion); ?>;
    var myData2 = JSON.parse(myData);
    myData2.shift();
    console.log(myData2);

    var jurisdicciones_nombres = [];
    var jurisdicciones_totales = [];

    var i;
    for (i = 0; i < myData2.length; i++)
    {
        var jurisdiccion = myData2[i]
        jurisdicciones_nombres.push(jurisdiccion[0]);
        jurisdicciones_totales.push(parseFloat(jurisdiccion[1].replace(/,/g , "")));
    };

    var data = {
        //labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        labels: jurisdicciones_nombres,
        datasets: [
            {
              label: "Presupuestado",
              backgroundColor: ["#2ea6c3", "#37967b", "#c01190", "#d7b200", "#c02042", "#b2bb38", "#ff5722", "#00e676", "#555"],
              data: jurisdicciones_totales
            }
        ]
    };

    var barChartCanvas = $('#barChart1').get(0).getContext('2d');

    var barChart = new Chart(barChartCanvas, {
        type: 'bar',
        data: data,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true,
                        callback: function (tick, index, ticks) {
                            return numeral(tick).format('0.0a');
                        }
                    }
                }],
                xAxes: [{
                    display: false
                }]
            },
            legend: {
                display: false
            },
            labels: {
                display: false
            },
            maintainAspectRatio: false
        }
    });

    myData = <?php echo json_encode($publicidad); ?>;
    myData2 = JSON.parse(myData);
    console.log(myData2);

    var publicidad_nombres = [];
    var publicidad_totales = [];

    var i;
    for (i = 0; i < myData2.length; i++)
    {
        var publicidad = myData2[i]
        publicidad_nombres.push(publicidad[0]);
        publicidad_totales.push(parseFloat(publicidad[1].replace(/,/g , "")));
    };

    var data = {
        labels: publicidad_nombres,
        datasets: [
            {
              backgroundColor: "#ff6f00",
              data: publicidad_totales
            }
        ]
    };

    var barChartCanvas = $('#barChart2').get(0).getContext('2d');

    var barChart = new Chart(barChartCanvas, {
        type: 'bar',
        data: data,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true,
                        callback: function (tick, index, ticks) {
                            return numeral(tick).format('0.0a');
                        }
                    }
                }],
                xAxes: [{
                    display: false
                }]
            },
            legend: {
                display: false
            },
            labels: {
                display: false
            },
            maintainAspectRatio: false
        }
    });

    myData = <?php echo json_encode($publicidad_total); ?>;
    myData2 = JSON.parse(myData);
    console.log(myData2);

</script>
@endsection

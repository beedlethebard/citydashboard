@extends('layouts.dashboards.app')

@section('dashboard-header')
    <section class="content-header">
      <h1>
        @lang('custom.section-6')
      </h1>
      <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-dashboard"></i> @lang('custom.home')</a></li>
        <li class="active">@lang('custom.section-6')</li>
      </ol>
    </section>
@endsection

@section('content')


@endsection

@section('footer')

@endsection

@section('data-scripts')

@endsection

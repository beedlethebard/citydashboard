@extends('layouts.dashboards.app')

@section('dashboard-header')
    <section class="content-header">
      <h1>
        @lang('custom.section-5')
      </h1>
      <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-dashboard"></i> @lang('custom.home')</a></li>
        <li class="active">@lang('custom.section-5')</li>
      </ol>
    </section>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-6">
      <div class="box box-home box-home-5">
        <div class="box-header with-border">
          <h3 class="box-title">Indicadores de resultado del programa de gestión de residuos sólidos del municipio</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
                <div class="chart-responsive pd-15">
                  <canvas id="lineChart1" height="250"></canvas>
                </div>
            </div>
          </div>
        </div>
        <div class="box-footer">
          <div class="description-block">
            <h5 class="description-percentage text-green">
              <span>
                <i class="fa fa-tag"></i>
              </span> @lang('custom.category') <strong>Medio Ambiente</strong>
            </h5>
            <span> @lang('custom.dataset') <strong>PROGR-DE-GESTI-DE-RESID</strong></span><br>
            <span> @lang('custom.last-mod') <strong class="FechaModificacion">30 de Octubre de 2017</strong></span>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('footer')

@endsection

@section('data-scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
  <script type="text/javascript">

    var myData = <?php echo json_encode($residuos_mes); ?>;
    var myData2 = JSON.parse(myData);
    myData2.shift();
    console.log(myData2);

    var residuos_mes = [];

    var i;
    for (i = 0; i < myData2.length; i++)
    {
        var total = myData2[i]
        residuos_mes.push(parseFloat(total[5].replace(/,/g , "")));
    };

    var sum = residuos_mes.reduce((a, b) => a + b, 0);
    console.log(sum); // 6

    var data = {
          labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviebre", "Diciembre"],
          datasets: [
            {
              label: "Toneladas en total",
              backgroundColor: "#00a43d",
              fill: false,
              data: residuos_mes
            }
        ]
    };

    var pieChartCanvas = $('#lineChart1').get(0).getContext('2d');

    var pieChart = new Chart(pieChartCanvas, {
        type: 'bar',
        data: data,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true,
                        callback: function (tick, index, ticks) {
                            return numeral(tick).format('0.0a');
                        }
                    }
                }],
                xAxes: [{
                    display: false
                }]
            },
            legend: {
                display: false
            },
            labels: {
                display: false
            },
            maintainAspectRatio: false
        }
    });


  </script>
@endsection

@extends('layouts.dashboards.app')

@section('dashboard-header')
    <section class="content-header">
      <h1>
        @lang('custom.section-2')
      </h1>
      <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-dashboard"></i> @lang('custom.home')</a></li>
        <li class="active">@lang('custom.section-2')</li>
      </ol>
    </section>
@endsection

@section('content')
<div class="row">

  <div class="col-md-3">
    <div class="info-box">
      <span class="info-box-icon bg-blue">2017</span>

      <div class="info-box-content">
        <span class="info-box-text">Presupuestado</span>
        <span id="presupuestado_total_2017" class="info-box-number"></span>
      </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="info-box">
      <span class="info-box-icon bg-red">2017</span>

      <div class="info-box-content">
        <span class="info-box-text">Ejecutado</span>
        <span id="ejecutado_total_2017" class="info-box-number"></span>

        <div class="progress">
          <div id="progressPorcentajeEjecutado_2017" class="progress-bar bg-red"></div>
        </div>
        <span id="porcentajeEjecutado_2017"class="progress-description"></span>
      </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="info-box">
      <span class="info-box-icon bg-blue">2016</span>

      <div class="info-box-content">
        <span class="info-box-text">Presupuestado</span>
        <span id="presupuestado_total_2016" class="info-box-number"></span>
      </div>
    </div>
  </div>

  <div class="col-md-3">
    <div class="info-box">
      <span class="info-box-icon bg-red">2016</span>

      <div class="info-box-content">
        <span class="info-box-text">Ejecutado</span>
        <span id="ejecutado_total_2016" class="info-box-number"></span>

        <div class="progress">
          <div id="progressPorcentajeEjecutado_2016" class="progress-bar bg-red"></div>
        </div>
        <span id="porcentajeEjecutado_2016"class="progress-description"></span>
      </div>
    </div>
  </div>

</div>

<div class="row">

  <div class="col-md-12">

    <div class="box box-home box-home-2">

      <div class="box-header with-border">
        <h3 class="box-title">@lang('custom.budgeted-executed-sec')</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-minus"></i>
          </button>
        </div>
      </div>

      <div class="box-body">
        <div class="row">

          <div class="col-md-6">
              <p class="text-center">
                <strong>@lang('custom.year-2017')</strong>
              </p>

              <div class="chart-responsive pd-15">
                <canvas id="pygPorSecretarias_2017" height="300"></canvas>
              </div>
          </div>
          <div class="col-md-6">
              <p class="text-center">
                <strong>@lang('custom.year-2016')</strong>
              </p>

              <div class="chart-responsive pd-15">
                <canvas id="pygPorSecretarias_2016" height="300"></canvas>
              </div>
          </div>

        </div>

      </div>

      <div class="box-footer">
        <div class="description-block">
          <h5 class="description-percentage text-blue">
            <span>
              <i class="fa fa-tag"></i>
            </span> @lang('custom.category') <strong class="Categoria"></strong>
          </h5>
          <span> @lang('custom.dataset') <strong class="Guid"></strong></span><br>
          <span> @lang('custom.last-mod') <strong class="FechaModificacion"></strong></span>
        </div>
      </div>

    </div>

  </div>

</div>

<div class="row">

  <div class="col-md-12">

    <div class="box box-home box-home-2">
      <div class="box-header with-border">
        <h3 class="box-title">@lang('custom.budget-perc-total')</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-minus"></i>
          </button>
        </div>
      </div>

      <div class="box-body">
        <div class="row">

          <div class="col-md-6">
              <p class="text-center">
                <strong>@lang('custom.year-2017')</strong>
              </p>

              <div class="progress-group">
                <span class="progress-text">Honorable Concejo Deliberante</span>
                <span class="progress-number"><b id="porcentajeHCD_2017_p"></b>% / <b id="porcentajeHCD_2017_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeHCD_2017" class="progress-bar progress-bar-concejo"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Instituto Cultural</span>
                <span class="progress-number"><b id="porcentajeCultura_2017_p"></b>% / <b id="porcentajeCultura_2017_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeCultura_2017" class="progress-bar progress-bar-cultura" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Intendencia</span>
                <span class="progress-number"><b id="porcentajeIntendencia_2017_p"></b>% / <b id="porcentajeIntendencia_2017_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeIntendencia_2017" class="progress-bar progress-bar-intendencia" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Asesoría Letrada</span>
                <span class="progress-number"><b id="porcentajeLetrada_2017_p"></b>% / <b id="porcentajeLetrada_2017_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeLetrada_2017" class="progress-bar progress-bar-asesoria-letrada" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Gestión Ambiental</span>
                <span class="progress-number"><b id="porcentajeAmbiental_2017_p"></b>% / <b id="porcentajeAmbiental_2017_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeAmbiental_2017" class="progress-bar progress-bar-gestion-ambiental" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Gobierno</span>
                <span class="progress-number"><b id="porcentajeGobierno_2017_p"></b>% / <b id="porcentajeGobierno_2017_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeGobierno_2017" class="progress-bar progress-bar-gobierno" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Hacienda y Desarrollo Económico</span>
                <span class="progress-number"><b id="porcentajeHacienda_2017_p"></b>% / <b id="porcentajeHacienda_2017_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeHacienda_2017" class="progress-bar progress-bar-hacienda" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Infraestructura</span>
                <span class="progress-number"><b id="porcentajeInfraestructura_2017_p"></b>% / <b id="porcentajeInfraestructura_2017_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeInfraestructura_2017" class="progress-bar progress-bar-infraestructura" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Innovación Tecnológica y Desarrollo Creativo</span>
                <span class="progress-number"><b id="porcentajeInnovacion_2017_p"></b>% / <b id="porcentajeInnovacion_2017_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeInnovacion_2017" class="progress-bar progress-bar-innovacion" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Modernización y Calidad de Gestión</span>
                <span class="progress-number"><b id="porcentajeModernizacion_2017_p"></b>% / <b id="porcentajeModernizacion_2017_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeModernizacion_2017" class="progress-bar progress-bar-modernizacion" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Políticas Sociales</span>
                <span class="progress-number"><b id="porcentajeSociales_2017_p"></b>% / <b id="porcentajeSociales_2017_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeSociales_2017" class="progress-bar progress-bar-politicas-sociales" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Salud</span>
                <span class="progress-number"><b id="porcentajeSalud_2017_p"></b>% / <b id="porcentajeSalud_2017_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeSalud_2017" class="progress-bar progress-bar-salud" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Seguridad y Protección Ciudadana</span>
                <span class="progress-number"><b id="porcentajeSeguridad_2017_p"></b>% / <b id="porcentajeSeguridad_2017_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeSeguridad_2017" class="progress-bar progress-bar-seguridad" style="width: 80%"></div>
                </div>
              </div>
          </div>

          <div class="col-md-6">
              <p class="text-center">
                <strong>@lang('custom.year-2016')</strong>
              </p>

              <div class="progress-group">
                <span class="progress-text">Honorable Concejo Deliberante</span>
                <span class="progress-number"><b id="porcentajeHCD_2016_p"></b>% / <b id="porcentajeHCD_2016_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeHCD_2016" class="progress-bar progress-bar-concejo"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Instituto Cultural</span>
                <span class="progress-number"><b id="porcentajeCultura_2016_p"></b>% / <b id="porcentajeCultura_2016_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeCultura_2016" class="progress-bar progress-bar-cultura" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Intendencia</span>
                <span class="progress-number"><b id="porcentajeIntendencia_2016_p"></b>% / <b id="porcentajeIntendencia_2016_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeIntendencia_2016" class="progress-bar progress-bar-intendencia" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Asesoría Letrada</span>
                <span class="progress-number"><b id="porcentajeLetrada_2016_p"></b>% / <b id="porcentajeLetrada_2016_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeLetrada_2016" class="progress-bar progress-bar-asesoria-letrada" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Gestión Ambiental</span>
                <span class="progress-number"><b id="porcentajeAmbiental_2016_p"></b>% / <b id="porcentajeAmbiental_2016_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeAmbiental_2016" class="progress-bar progress-bar-gestion-ambiental" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Gobierno</span>
                <span class="progress-number"><b id="porcentajeGobierno_2016_p"></b>% / <b id="porcentajeGobierno_2016_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeGobierno_2016" class="progress-bar progress-bar-gobierno" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Hacienda y Desarrollo Económico</span>
                <span class="progress-number"><b id="porcentajeHacienda_2016_p"></b>% / <b id="porcentajeHacienda_2016_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeHacienda_2016" class="progress-bar progress-bar-hacienda" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Infraestructura</span>
                <span class="progress-number"><b id="porcentajeInfraestructura_2016_p"></b>% / <b id="porcentajeInfraestructura_2016_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeInfraestructura_2016" class="progress-bar progress-bar-infraestructura" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Innovación Tecnológica y Desarrollo Creativo</span>
                <span class="progress-number"><b id="porcentajeInnovacion_2016_p"></b>% / <b id="porcentajeInnovacion_2016_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeInnovacion_2016" class="progress-bar progress-bar-innovacion" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Modernización y Calidad de Gestión</span>
                <span class="progress-number"><b id="porcentajeModernizacion_2016_p"></b>% / <b id="porcentajeModernizacion_2016_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeModernizacion_2016" class="progress-bar progress-bar-modernizacion" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Políticas Sociales</span>
                <span class="progress-number"><b id="porcentajeSociales_2016_p"></b>% / <b id="porcentajeSociales_2016_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeSociales_2016" class="progress-bar progress-bar-politicas-sociales" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Salud</span>
                <span class="progress-number"><b id="porcentajeSalud_2016_p"></b>% / <b id="porcentajeSalud_2016_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeSalud_2016" class="progress-bar progress-bar-salud" style="width: 80%"></div>
                </div>
              </div>

              <div class="progress-group">
                <span class="progress-text">Secretaría de Seguridad y Protección Ciudadana</span>
                <span class="progress-number"><b id="porcentajeSeguridad_2016_p"></b>% / <b id="porcentajeSeguridad_2016_t"></b>%</span>

                <div class="progress sm">
                  <div id="progressPorcentajeSeguridad_2016" class="progress-bar progress-bar-seguridad" style="width: 80%"></div>
                </div>
              </div>
          </div>
        </div>
      </div>
      <div class="box-footer">
        <div class="description-block">
          <h5 class="description-percentage text-blue">
            <span>
              <i class="fa fa-tag"></i>
            </span> @lang('custom.category') <strong class="Categoria"></strong>
          </h5>
          <span> @lang('custom.dataset') <strong class="Guid"></strong></span><br>
          <span> @lang('custom.last-mod') <strong class="FechaModificacion"></strong></span>
        </div>
      </div>
    </div>
  </div>

</div>


<div class="row">

  <div class="col-md-6">
    <div class="box box-home box-home-2">
      <div class="box-header with-border">
        <h3 class="box-title">@lang('custom.budget-exec-2017')</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
              <div class="chart-responsive pd-15">
                <canvas id="pygPorSecretariasPolar1" height="250"></canvas>
              </div>
          </div>
        </div>
      </div>
      <div class="box-footer">
        <div class="description-block">
          <h5 class="description-percentage text-blue">
            <span>
              <i class="fa fa-tag"></i>
            </span> @lang('custom.category') <strong class="Categoria"></strong>
          </h5>
          <span> @lang('custom.dataset') <strong class="Guid"></strong></span><br>
          <span> @lang('custom.last-mod') <strong class="FechaModificacion"></strong></span>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="box box-home box-home-2">
      <div class="box-header with-border">
        <h3 class="box-title">@lang('custom.budget-exec-2016')</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse">
            <i class="fa fa-minus"></i>
          </button>
        </div>
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <div class="chart-responsive pd-15">
              <canvas id="pygPorSecretariasPolar2" height="250"></canvas>
            </div>
          </div>
        </div>
      </div>
      <div class="box-footer">
        <div class="description-block">
          <h5 class="description-percentage text-blue">
            <span>
              <i class="fa fa-tag"></i>
            </span> @lang('custom.category') <strong class="Categoria"></strong>
          </h5>
          <span> @lang('custom.dataset') <strong class="Guid"></strong></span><br>
          <span> @lang('custom.last-mod') <strong class="FechaModificacion"></strong></span>
        </div>
      </div>
    </div>
  </div>


</div>
@endsection

@section('footer')

@endsection

@section('data-scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
  <script type="text/javascript">
    $(function () {
        'use strict';

        var secretarias_2017 = <?php echo json_encode($secretarias_2017); ?>;
        var secretarias_2017 = JSON.parse(secretarias_2017); // console.log(secretarias_2017);

        var secretarias_nombres = [];
        var secretarias_2017_presupuestado = [];
        var secretarias_2017_ejecutado = [];

        var i;
        for (i = 0; i < secretarias_2017.length; i++)
        {
            var dependencia = secretarias_2017[i]
            secretarias_nombres.push(dependencia[0]);
            secretarias_2017_presupuestado.push(parseFloat(dependencia[1].replace(/,/g , "")));
            secretarias_2017_ejecutado.push(parseFloat(dependencia[2].replace(/,/g , "")));
        };

        var data1 = {
            labels: secretarias_nombres,
            datasets: [
                {
                  label: "Presupuestado",
                  type: "line",
                  borderColor: "#0073b7",
                  data: secretarias_2017_presupuestado,
                  fill: false
                },
                {
                  label: "Ejecutado",
                  type: "line",
                  borderColor: "#dd4b39",
                  data: secretarias_2017_ejecutado,
                  fill: false
                },
                {
                  label: "Presupuestado",
                  type: "bar",
                  backgroundColor: ["#3e95cd", "#c02042", "#555", "#b32666", "#00a43d", "#37967b", "#b2bb38", "#2ea6c3", "#e3007a", "#e43117", "#c01190", "#d7b200", "#2865be"],
                  data: secretarias_2017_presupuestado
                },
                {
                  label: "Ejecutado",
                  type: "bar",
                  backgroundColor: ["#3e95cd", "#c02042", "#555", "#b32666", "#00a43d", "#37967b", "#b2bb38", "#2ea6c3", "#e3007a", "#e43117", "#c01190", "#d7b200", "#2865be"],
                  data: secretarias_2017_ejecutado
                }
            ]
        };


        var mixedCanvas = $('#pygPorSecretarias_2017').get(0).getContext('2d');

        var mixedChart = new Chart(mixedCanvas, {
            type: 'bar',
            data: data1,
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            callback: function (tick, index, ticks) {
                                return numeral(tick).format('0.0a');
                                // Return string here.
                                // Return empty string to have no label with a grid line
                                // Return null or undefined to hide the gridline
                            }
                        }
                    }],
                    xAxes: [{
                        display: false
                    }]
                },
                legend: {
                    display: false
                },
                labels: {
                  display: false
                },
                maintainAspectRatio: false
            }
        });

        var polarChartCanvas = $('#pygPorSecretariasPolar1').get(0).getContext('2d');

        var data2 = {
              labels: secretarias_nombres,
              datasets: [
                {
                  label: "Presupuestado",
                  backgroundColor: ["#3e95cd", "#c02042", "#555", "#b32666", "#00a43d", "#37967b", "#b2bb38", "#2ea6c3", "#e3007a", "#e43117", "#c01190", "#d7b200", "#2865be"],
                  data: secretarias_2017_ejecutado
                }
            ]
        };

        var polarArea = new Chart(polarChartCanvas, {
            type: 'polarArea',
            data: data2,
            options: {
                scales: {
                  ticks: {
                    stepSize: 1
                  }
                },
                legend: {
                    display: false
                },
                labels: {
                  display: false
                }
            }
        });


        /* ----------------------------------------------------------------- */

        var categoria = <?php echo json_encode($category); ?>;
        $('.Categoria').text(categoria);

        /* ----------------------------------------------------------------- */

        var guid = <?php echo json_encode($guid); ?>;
        $('.Guid').text(guid);

        /* ----------------------------------------------------------------- */

        var fecha = <?php echo json_encode($modified_at); ?>; // console.log(fecha);
        var fecha2 = new Date(fecha.replace(/"/g , ""));      // console.log(fecha2);
        var optionsFecha = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
        $('.FechaModificacion').text(fecha2.toLocaleString('es-AR', optionsFecha)); // console.log(fecha2.toLocaleString('es-AR', optionsFecha));

        /* ----------------------------------------------------------------- */

        var municipio_2017 = <?php echo json_encode($municipio_2017); ?>;
        municipio_2017 = JSON.parse(municipio_2017);

        var presupuestado_total_2017 = municipio_2017[0].split(".");
        presupuestado_total_2017 = presupuestado_total_2017[0].replace(/,/g , ".");

        var ejecutado_total_2017 = municipio_2017[1].split(".");
        ejecutado_total_2017 = ejecutado_total_2017[0].replace(/,/g , ".");

        $('#presupuestado_total_2017').text("$ ".concat(presupuestado_total_2017));
        $('#ejecutado_total_2017').text("$ ".concat(ejecutado_total_2017));

        var intPresupuestado_total_2017 = parseInt(presupuestado_total_2017.replace(/\./g , ""));
        var intEjecutado_total_2017 = parseInt(ejecutado_total_2017.replace(/\./g , ""));

        var porcentajeEjecutado_2017 = parseInt(intEjecutado_total_2017 * 100 / intPresupuestado_total_2017);
        $('#porcentajeEjecutado_2017').text("≈ ".concat(porcentajeEjecutado_2017.toString().concat(" % del presupuesto")));
        $('#progressPorcentajeEjecutado_2017').css('width', porcentajeEjecutado_2017.toString().concat("%"));

        var porcentajeEjecutadoHCD_2017_t = parseFloat(secretarias_2017_ejecutado[0] * 100 / intEjecutado_total_2017).toFixed(2);
        $('#porcentajeHCD_2017_t').text(porcentajeEjecutadoHCD_2017_t.toString());
        $('#progressPorcentajeHCD_2017').css('width', porcentajeEjecutadoHCD_2017_t.toString().concat("%"));
        var porcentajeEjecutadoHCD_2017_p = parseFloat(secretarias_2017_ejecutado[0] * 100 / secretarias_2017_presupuestado[0]).toFixed(2);
        $('#porcentajeHCD_2017_p').text(porcentajeEjecutadoHCD_2017_p.toString());

        var porcentajeEjecutadoCultura_2017_t = parseFloat(secretarias_2017_ejecutado[1] * 100 / intEjecutado_total_2017).toFixed(2);
        $('#porcentajeCultura_2017_t').text(porcentajeEjecutadoCultura_2017_t.toString());
        $('#progressPorcentajeCultura_2017').css('width', porcentajeEjecutadoCultura_2017_t.toString().concat("%"));
        var porcentajeEjecutadoCultura_2017_p = parseFloat(secretarias_2017_ejecutado[1] * 100 / secretarias_2017_presupuestado[1]).toFixed(2);
        $('#porcentajeCultura_2017_p').text(porcentajeEjecutadoCultura_2017_p.toString());

        var porcentajeEjecutadoIntendencia_2017_t = parseFloat(secretarias_2017_ejecutado[2] * 100 / intEjecutado_total_2017).toFixed(2);
        $('#porcentajeIntendencia_2017_t').text(porcentajeEjecutadoIntendencia_2017_t.toString());
        $('#progressPorcentajeIntendencia_2017').css('width', porcentajeEjecutadoIntendencia_2017_t.toString().concat("%"));
        var porcentajeEjecutadoIntendencia_2017_p = parseFloat(secretarias_2017_ejecutado[2] * 100 / secretarias_2017_presupuestado[2]).toFixed(2);
        $('#porcentajeIntendencia_2017_p').text(porcentajeEjecutadoIntendencia_2017_p.toString());

        var porcentajeEjecutadoLetrada_2017_t = parseFloat(secretarias_2017_ejecutado[3] * 100 / intEjecutado_total_2017).toFixed(2);
        $('#porcentajeLetrada_2017_t').text(porcentajeEjecutadoLetrada_2017_t.toString());
        $('#progressPorcentajeLetrada_2017').css('width', porcentajeEjecutadoLetrada_2017_t.toString().concat("%"));
        var porcentajeEjecutadoLetrada_2017_p = parseFloat(secretarias_2017_ejecutado[3] * 100 / secretarias_2017_presupuestado[3]).toFixed(2);
        $('#porcentajeLetrada_2017_p').text(porcentajeEjecutadoLetrada_2017_p.toString());

        var porcentajeEjecutadoAmbiental_2017_t = parseFloat(secretarias_2017_ejecutado[4] * 100 / intEjecutado_total_2017).toFixed(2);
        $('#porcentajeAmbiental_2017_t').text(porcentajeEjecutadoAmbiental_2017_t.toString());
        $('#progressPorcentajeAmbiental_2017').css('width', porcentajeEjecutadoAmbiental_2017_t.toString().concat("%"));
        var porcentajeEjecutadoAmbiental_2017_p = parseFloat(secretarias_2017_ejecutado[4] * 100 / secretarias_2017_presupuestado[4]).toFixed(2);
        $('#porcentajeAmbiental_2017_p').text(porcentajeEjecutadoAmbiental_2017_p.toString());

        var porcentajeEjecutadoGobierno_2017_t = parseFloat(secretarias_2017_ejecutado[5] * 100 / intEjecutado_total_2017).toFixed(2);
        $('#porcentajeGobierno_2017_t').text(porcentajeEjecutadoGobierno_2017_t.toString());
        $('#progressPorcentajeGobierno_2017').css('width', porcentajeEjecutadoGobierno_2017_t.toString().concat("%"));
        var porcentajeEjecutadoGobierno_2017_p = parseFloat(secretarias_2017_ejecutado[5] * 100 / secretarias_2017_presupuestado[5]).toFixed(2);
        $('#porcentajeGobierno_2017_p').text(porcentajeEjecutadoGobierno_2017_p.toString());

        var porcentajeEjecutadoHacienda_2017_t = parseFloat(secretarias_2017_ejecutado[6] * 100 / intEjecutado_total_2017).toFixed(2);
        $('#porcentajeHacienda_2017_t').text(porcentajeEjecutadoHacienda_2017_t.toString());
        $('#progressPorcentajeHacienda_2017').css('width', porcentajeEjecutadoHacienda_2017_t.toString().concat("%"));
        var porcentajeEjecutadoHacienda_2017_p = parseFloat(secretarias_2017_ejecutado[6] * 100 / secretarias_2017_presupuestado[6]).toFixed(2);
        $('#porcentajeHacienda_2017_p').text(porcentajeEjecutadoHacienda_2017_p.toString());

        var porcentajeEjecutadoInfraestructura_2017_t = parseFloat(secretarias_2017_ejecutado[7] * 100 / intEjecutado_total_2017).toFixed(2);
        $('#porcentajeInfraestructura_2017_t').text(porcentajeEjecutadoInfraestructura_2017_t.toString());
        $('#progressPorcentajeInfraestructura_2017').css('width', porcentajeEjecutadoInfraestructura_2017_t.toString().concat("%"));
        var porcentajeEjecutadoInfraestructura_2017_p = parseFloat(secretarias_2017_ejecutado[7] * 100 / secretarias_2017_presupuestado[7]).toFixed(2);
        $('#porcentajeInfraestructura_2017_p').text(porcentajeEjecutadoInfraestructura_2017_p.toString());

        var porcentajeEjecutadoInnovacion_2017_t = parseFloat(secretarias_2017_ejecutado[8] * 100 / intEjecutado_total_2017).toFixed(2);
        $('#porcentajeInnovacion_2017_t').text(porcentajeEjecutadoInnovacion_2017_t.toString());
        $('#progressPorcentajeInnovacion_2017').css('width', porcentajeEjecutadoInnovacion_2017_t.toString().concat("%"));
        var porcentajeEjecutadoInnovacion_2017_p = parseFloat(secretarias_2017_ejecutado[8] * 100 / secretarias_2017_presupuestado[8]).toFixed(2);
        $('#porcentajeInnovacion_2017_p').text(porcentajeEjecutadoInnovacion_2017_p.toString());

        var porcentajeEjecutadoModernizacion_2017_t = parseFloat(secretarias_2017_ejecutado[9] * 100 / intEjecutado_total_2017).toFixed(2);
        $('#porcentajeModernizacion_2017_t').text(porcentajeEjecutadoModernizacion_2017_t.toString());
        $('#progressPorcentajeModernizacion_2017').css('width', porcentajeEjecutadoModernizacion_2017_t.toString().concat("%"));
        var porcentajeEjecutadoModernizacion_2017_p = parseFloat(secretarias_2017_ejecutado[9] * 100 / secretarias_2017_presupuestado[9]).toFixed(2);
        $('#porcentajeModernizacion_2017_p').text(porcentajeEjecutadoModernizacion_2017_p.toString());

        var porcentajeEjecutadoSociales_2017_t = parseFloat(secretarias_2017_ejecutado[10] * 100 / intEjecutado_total_2017).toFixed(2);
        $('#porcentajeSociales_2017_t').text(porcentajeEjecutadoSociales_2017_t.toString());
        $('#progressPorcentajeSociales_2017').css('width', porcentajeEjecutadoSociales_2017_t.toString().concat("%"));
        var porcentajeEjecutadoSociales_2017_p = parseFloat(secretarias_2017_ejecutado[10] * 100 / secretarias_2017_presupuestado[10]).toFixed(2);
        $('#porcentajeSociales_2017_p').text(porcentajeEjecutadoSociales_2017_p.toString());

        var porcentajeEjecutadoSalud_2017_t = parseFloat(secretarias_2017_ejecutado[11] * 100 / intEjecutado_total_2017).toFixed(2);
        $('#porcentajeSalud_2017_t').text(porcentajeEjecutadoSalud_2017_t.toString());
        $('#progressPorcentajeSalud_2017').css('width', porcentajeEjecutadoSalud_2017_t.toString().concat("%"));
        var porcentajeEjecutadoSalud_2017_p = parseFloat(secretarias_2017_ejecutado[11] * 100 / secretarias_2017_presupuestado[11]).toFixed(2);
        $('#porcentajeSalud_2017_p').text(porcentajeEjecutadoSalud_2017_p.toString());

        var porcentajeEjecutadoSeguridad_2017_t = parseFloat(secretarias_2017_ejecutado[12] * 100 / intEjecutado_total_2017).toFixed(2);
        $('#porcentajeSeguridad_2017_t').text(porcentajeEjecutadoSeguridad_2017_t.toString());
        $('#progressPorcentajeSeguridad_2017').css('width', porcentajeEjecutadoSeguridad_2017_t.toString().concat("%"));
        var porcentajeEjecutadoSeguridad_2017_p = parseFloat(secretarias_2017_ejecutado[12] * 100 / secretarias_2017_presupuestado[12]).toFixed(2);
        $('#porcentajeSeguridad_2017_p').text(porcentajeEjecutadoSeguridad_2017_p.toString());
    });
  </script>

  <script type="text/javascript">
    $(function () {
        'use strict';

        var secretarias_2016 = <?php echo json_encode($secretarias_2016); ?>;
        var secretarias_2016 = JSON.parse(secretarias_2016); // console.log(secretarias_2016);

        var secretarias_nombres = [];
        var secretarias_2016_presupuestado = [];
        var secretarias_2016_ejecutado = [];

        var i;
        for (i = 0; i < secretarias_2016.length; i++)
        {
            var dependencia = secretarias_2016[i]
            secretarias_nombres.push(dependencia[0]);
            secretarias_2016_presupuestado.push(parseFloat(dependencia[1].replace(/,/g , "")));
            secretarias_2016_ejecutado.push(parseFloat(dependencia[2].replace(/,/g , "")));
        };

        var data1 = {
            labels: secretarias_nombres,
            datasets: [
                {
                  label: "Presupuestado",
                  type: "line",
                  borderColor: "#0073b7",
                  data: secretarias_2016_presupuestado,
                  fill: false
                },
                {
                  label: "Ejecutado",
                  type: "line",
                  borderColor: "#dd4b39",
                  data: secretarias_2016_ejecutado,
                  fill: false
                },
                {
                  label: "Presupuestado",
                  type: "bar",
                  backgroundColor: ["#3e95cd", "#c02042", "#555", "#b32666", "#00a43d", "#37967b", "#b2bb38", "#2ea6c3", "#e3007a", "#e43117", "#c01190", "#d7b200", "#2865be"],
                  data: secretarias_2016_presupuestado
                },
                {
                  label: "Ejecutado",
                  type: "bar",
                  backgroundColor: ["#3e95cd", "#c02042", "#555", "#b32666", "#00a43d", "#37967b", "#b2bb38", "#2ea6c3", "#e3007a", "#e43117", "#c01190", "#d7b200", "#2865be"],
                  data: secretarias_2016_ejecutado
                }
            ]
        };


        var mixedCanvas = $('#pygPorSecretarias_2016').get(0).getContext('2d');

        var mixedChart = new Chart(mixedCanvas, {
            type: 'bar',
            data: data1,
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            callback: function (tick, index, ticks) {
                                return numeral(tick).format('0.0a');
                                // Return string here.
                                // Return empty string to have no label with a grid line
                                // Return null or undefined to hide the gridline
                            }
                        }
                    }],
                    xAxes: [{
                        display: false
                    }]
                },
                legend: {
                    display: false
                },
                labels: {
                  display: false
                },
                maintainAspectRatio: false
            }
        });

        var polarChartCanvas = $('#pygPorSecretariasPolar2').get(0).getContext('2d');

        var data2 = {
              labels: secretarias_nombres,
              datasets: [
                {
                  label: "Presupuestado",
                  backgroundColor: ["#3e95cd", "#c02042", "#555", "#b32666", "#00a43d", "#37967b", "#b2bb38", "#2ea6c3", "#e3007a", "#e43117", "#c01190", "#d7b200", "#2865be"],
                  data: secretarias_2016_ejecutado
                }
            ]
        };

        var polarArea = new Chart(polarChartCanvas, {
            type: 'polarArea',
            data: data2,
            options: {
                scales: {
                  ticks: {
                    stepSize: 1
                  }
                },
                legend: {
                    display: false
                },
                labels: {
                  display: false
                }
            }
        });

        /* ----------------------------------------------------------------- */

        var categoria = <?php echo json_encode($category); ?>;
        $('.Categoria').text(categoria);

        /* ----------------------------------------------------------------- */

        var guid = <?php echo json_encode($guid); ?>;
        $('.Guid').text(guid);

        /* ----------------------------------------------------------------- */

        var fecha = <?php echo json_encode($modified_at); ?>; // console.log(fecha);
        var fecha2 = new Date(fecha.replace(/"/g , ""));      // console.log(fecha2);
        var optionsFecha = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
        $('.FechaModificacion').text(fecha2.toLocaleString('es-AR', optionsFecha)); // console.log(fecha2.toLocaleString('es-AR', optionsFecha));

        /* ----------------------------------------------------------------- */

        var municipio_2016 = <?php echo json_encode($municipio_2016); ?>;
        municipio_2016 = JSON.parse(municipio_2016);

        var presupuestado_total_2016 = municipio_2016[0].split(".");
        presupuestado_total_2016 = presupuestado_total_2016[0].replace(/,/g , ".");

        var ejecutado_total_2016 = municipio_2016[1].split(".");
        ejecutado_total_2016 = ejecutado_total_2016[0].replace(/,/g , ".");

        $('#presupuestado_total_2016').text("$ ".concat(presupuestado_total_2016));
        $('#ejecutado_total_2016').text("$ ".concat(ejecutado_total_2016));

        var intPresupuestado_total_2016 = parseInt(presupuestado_total_2016.replace(/\./g , ""));
        var intEjecutado_total_2016 = parseInt(ejecutado_total_2016.replace(/\./g , ""));

        var porcentajeEjecutado_2016 = parseInt(intEjecutado_total_2016 * 100 / intPresupuestado_total_2016);
        $('#porcentajeEjecutado_2016').text("≈ ".concat(porcentajeEjecutado_2016.toString().concat(" % del presupuesto")));
        $('#progressPorcentajeEjecutado_2016').css('width', porcentajeEjecutado_2016.toString().concat("%"));

        var porcentajeEjecutadoHCD_2016_t = parseFloat(secretarias_2016_ejecutado[0] * 100 / intEjecutado_total_2016).toFixed(2);
        $('#porcentajeHCD_2016_t').text(porcentajeEjecutadoHCD_2016_t.toString());
        $('#progressPorcentajeHCD_2016').css('width', porcentajeEjecutadoHCD_2016_t.toString().concat("%"));
        var porcentajeEjecutadoHCD_2016_p = parseFloat(secretarias_2016_ejecutado[0] * 100 / secretarias_2016_presupuestado[0]).toFixed(2);
        $('#porcentajeHCD_2016_p').text(porcentajeEjecutadoHCD_2016_p.toString());

        var porcentajeEjecutadoCultura_2016_t = parseFloat(secretarias_2016_ejecutado[1] * 100 / intEjecutado_total_2016).toFixed(2);
        $('#porcentajeCultura_2016_t').text(porcentajeEjecutadoCultura_2016_t.toString());
        $('#progressPorcentajeCultura_2016').css('width', porcentajeEjecutadoCultura_2016_t.toString().concat("%"));
        var porcentajeEjecutadoCultura_2016_p = parseFloat(secretarias_2016_ejecutado[1] * 100 / secretarias_2016_presupuestado[1]).toFixed(2);
        $('#porcentajeCultura_2016_p').text(porcentajeEjecutadoCultura_2016_p.toString());

        var porcentajeEjecutadoIntendencia_2016_t = parseFloat(secretarias_2016_ejecutado[2] * 100 / intEjecutado_total_2016).toFixed(2);
        $('#porcentajeIntendencia_2016_t').text(porcentajeEjecutadoIntendencia_2016_t.toString());
        $('#progressPorcentajeIntendencia_2016').css('width', porcentajeEjecutadoIntendencia_2016_t.toString().concat("%"));
        var porcentajeEjecutadoIntendencia_2016_p = parseFloat(secretarias_2016_ejecutado[2] * 100 / secretarias_2016_presupuestado[2]).toFixed(2);
        $('#porcentajeIntendencia_2016_p').text(porcentajeEjecutadoIntendencia_2016_p.toString());

        var porcentajeEjecutadoLetrada_2016_t = parseFloat(secretarias_2016_ejecutado[3] * 100 / intEjecutado_total_2016).toFixed(2);
        $('#porcentajeLetrada_2016_t').text(porcentajeEjecutadoLetrada_2016_t.toString());
        $('#progressPorcentajeLetrada_2016').css('width', porcentajeEjecutadoLetrada_2016_t.toString().concat("%"));
        var porcentajeEjecutadoLetrada_2016_p = parseFloat(secretarias_2016_ejecutado[3] * 100 / secretarias_2016_presupuestado[3]).toFixed(2);
        $('#porcentajeLetrada_2016_p').text(porcentajeEjecutadoLetrada_2016_p.toString());

        var porcentajeEjecutadoAmbiental_2016_t = parseFloat(secretarias_2016_ejecutado[4] * 100 / intEjecutado_total_2016).toFixed(2);
        $('#porcentajeAmbiental_2016_t').text(porcentajeEjecutadoAmbiental_2016_t.toString());
        $('#progressPorcentajeAmbiental_2016').css('width', porcentajeEjecutadoAmbiental_2016_t.toString().concat("%"));
        var porcentajeEjecutadoAmbiental_2016_p = parseFloat(secretarias_2016_ejecutado[4] * 100 / secretarias_2016_presupuestado[4]).toFixed(2);
        $('#porcentajeAmbiental_2016_p').text(porcentajeEjecutadoAmbiental_2016_p.toString());

        var porcentajeEjecutadoGobierno_2016_t = parseFloat(secretarias_2016_ejecutado[5] * 100 / intEjecutado_total_2016).toFixed(2);
        $('#porcentajeGobierno_2016_t').text(porcentajeEjecutadoGobierno_2016_t.toString());
        $('#progressPorcentajeGobierno_2016').css('width', porcentajeEjecutadoGobierno_2016_t.toString().concat("%"));
        var porcentajeEjecutadoGobierno_2016_p = parseFloat(secretarias_2016_ejecutado[5] * 100 / secretarias_2016_presupuestado[5]).toFixed(2);
        $('#porcentajeGobierno_2016_p').text(porcentajeEjecutadoGobierno_2016_p.toString());

        var porcentajeEjecutadoHacienda_2016_t = parseFloat(secretarias_2016_ejecutado[6] * 100 / intEjecutado_total_2016).toFixed(2);
        $('#porcentajeHacienda_2016_t').text(porcentajeEjecutadoHacienda_2016_t.toString());
        $('#progressPorcentajeHacienda_2016').css('width', porcentajeEjecutadoHacienda_2016_t.toString().concat("%"));
        var porcentajeEjecutadoHacienda_2016_p = parseFloat(secretarias_2016_ejecutado[6] * 100 / secretarias_2016_presupuestado[6]).toFixed(2);
        $('#porcentajeHacienda_2016_p').text(porcentajeEjecutadoHacienda_2016_p.toString());

        var porcentajeEjecutadoInfraestructura_2016_t = parseFloat(secretarias_2016_ejecutado[7] * 100 / intEjecutado_total_2016).toFixed(2);
        $('#porcentajeInfraestructura_2016_t').text(porcentajeEjecutadoInfraestructura_2016_t.toString());
        $('#progressPorcentajeInfraestructura_2016').css('width', porcentajeEjecutadoInfraestructura_2016_t.toString().concat("%"));
        var porcentajeEjecutadoInfraestructura_2016_p = parseFloat(secretarias_2016_ejecutado[7] * 100 / secretarias_2016_presupuestado[7]).toFixed(2);
        $('#porcentajeInfraestructura_2016_p').text(porcentajeEjecutadoInfraestructura_2016_p.toString());

        var porcentajeEjecutadoInnovacion_2016_t = parseFloat(secretarias_2016_ejecutado[8] * 100 / intEjecutado_total_2016).toFixed(2);
        $('#porcentajeInnovacion_2016_t').text(porcentajeEjecutadoInnovacion_2016_t.toString());
        $('#progressPorcentajeInnovacion_2016').css('width', porcentajeEjecutadoInnovacion_2016_t.toString().concat("%"));
        var porcentajeEjecutadoInnovacion_2016_p = parseFloat(secretarias_2016_ejecutado[8] * 100 / secretarias_2016_presupuestado[8]).toFixed(2);
        $('#porcentajeInnovacion_2016_p').text(porcentajeEjecutadoInnovacion_2016_p.toString());

        var porcentajeEjecutadoModernizacion_2016_t = parseFloat(secretarias_2016_ejecutado[9] * 100 / intEjecutado_total_2016).toFixed(2);
        $('#porcentajeModernizacion_2016_t').text(porcentajeEjecutadoModernizacion_2016_t.toString());
        $('#progressPorcentajeModernizacion_2016').css('width', porcentajeEjecutadoModernizacion_2016_t.toString().concat("%"));
        var porcentajeEjecutadoModernizacion_2016_p = parseFloat(secretarias_2016_ejecutado[9] * 100 / secretarias_2016_presupuestado[9]).toFixed(2);
        $('#porcentajeModernizacion_2016_p').text(porcentajeEjecutadoModernizacion_2016_p.toString());

        var porcentajeEjecutadoSociales_2016_t = parseFloat(secretarias_2016_ejecutado[10] * 100 / intEjecutado_total_2016).toFixed(2);
        $('#porcentajeSociales_2016_t').text(porcentajeEjecutadoSociales_2016_t.toString());
        $('#progressPorcentajeSociales_2016').css('width', porcentajeEjecutadoSociales_2016_t.toString().concat("%"));
        var porcentajeEjecutadoSociales_2016_p = parseFloat(secretarias_2016_ejecutado[10] * 100 / secretarias_2016_presupuestado[10]).toFixed(2);
        $('#porcentajeSociales_2016_p').text(porcentajeEjecutadoSociales_2016_p.toString());

        var porcentajeEjecutadoSalud_2016_t = parseFloat(secretarias_2016_ejecutado[11] * 100 / intEjecutado_total_2016).toFixed(2);
        $('#porcentajeSalud_2016_t').text(porcentajeEjecutadoSalud_2016_t.toString());
        $('#progressPorcentajeSalud_2016').css('width', porcentajeEjecutadoSalud_2016_t.toString().concat("%"));
        var porcentajeEjecutadoSalud_2016_p = parseFloat(secretarias_2016_ejecutado[11] * 100 / secretarias_2016_presupuestado[11]).toFixed(2);
        $('#porcentajeSalud_2016_p').text(porcentajeEjecutadoSalud_2016_p.toString());

        var porcentajeEjecutadoSeguridad_2016_t = parseFloat(secretarias_2016_ejecutado[12] * 100 / intEjecutado_total_2016).toFixed(2);
        $('#porcentajeSeguridad_2016_t').text(porcentajeEjecutadoSeguridad_2016_t.toString());
        $('#progressPorcentajeSeguridad_2016').css('width', porcentajeEjecutadoSeguridad_2016_t.toString().concat("%"));
        var porcentajeEjecutadoSeguridad_2016_p = parseFloat(secretarias_2016_ejecutado[12] * 100 / secretarias_2016_presupuestado[12]).toFixed(2);
        $('#porcentajeSeguridad_2016_p').text(porcentajeEjecutadoSeguridad_2016_p.toString());
    });
  </script>
@endsection

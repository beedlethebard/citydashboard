@extends('layouts.dashboards.app')

@section('dashboard-header')
    <section class="content-header">
      <h1>
        @lang('custom.section-1')
      </h1>
      <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-dashboard"></i> @lang('custom.home')</a></li>
        <li class="active">@lang('custom.section-1')</li>
      </ol>
    </section>
@endsection

@section('content')
  <!-- Main content -->
  <!-- Small boxes (Stat box) -->
  <div class="row">

      <div class="col-sm-7 col-lg-6">
        <div class="box box-home box-home-1">
          <div class="box-body text-center" style="padding: 0px;">
            <div id="organigrama">
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-5 col-lg-6">
        <div class="box box-home box-home-1">
          <div class="box-header with-border text-center">
            <h3 class="box-title">@lang('custom.proportion')</h3>
          </div>
          <form role="form">
            <div class="box-body pd-20">
              <label class="radio-inline control-label input-prop" for="uniforme">
                <input type="radio" name="mode" id="uniforme" value="count" checked> @lang('custom.uniform-prop')
              </label>
              <label class="radio-inline control-label input-prop pull-right" for="segunGasto">
                <input type="radio" name="mode" id="segunGasto" value="size"> @lang('custom.expenses-prop')
              </label>
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-md-8">
                  <div class="description-block">
                    <span> @lang('custom.dataset')
                      <strong>ORGAN</strong>
                    </span><br>
                    <span> @lang('custom.last-mod')
                      <strong id="fechaaa">viernes, 27 de enero de 2017</strong>
                    </span>
                    <h5 class="description-percentage text-orange">
                      <span>
                        <i class="fa fa-tag"></i>
                      </span> @lang('custom.category') <strong>Transparencia</strong>
                    </h5>
                  </div>
                </div>
                <div class="col-md-4" id="porcentaje">
                  <blockquote>
                    <p class="lead"><strong id="numero"></strong></p>
                    <small id="quote"></small>
                  </blockquote>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="box box-widget widget-user box-home box-home-1">
          <div class="widget-user-header pd-5" >
            <img id="imglogo" src="{{asset('img/muni/municipio.png') }}" class="widget-dependency-header" alt="">
          </div>
          <div class="box-footer">
            <div class="row">
              <div class="col-12">
                <div class="description-block">
                  <h5 class="description-header description-header-1">@lang('custom.dependency')</h5>
                  <span id="dependencia" class="description-text description-text-1">_</span>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="description-block">
                  <h5 class="description-header description-header-1">@lang('custom.representative')</h5>
                  <span id="responsable" class="description-text description-text-sm-1">_</span>
                </div>
                <!-- /.description-block -->
              </div>
              <div class="col-sm-6">
                <div class="description-block">
                  <h5 class="description-header description-header-1">@lang('custom.position')</h5>
                  <span id="cargo" class="description-text description-text-sm-1">_</span>
                </div>
                <!-- /.description-block -->
              </div>
            </div>
            <!-- /.row -->
          </div>
        </div>
      </div>

  </div>

@endsection

@section('footer')

@endsection

@section('data-scripts')
<script src="{{ asset('js/d3.v3.min.js') }}"></script>
<script src="{{ asset('js/organigrama.js') }}"></script>
@endsection

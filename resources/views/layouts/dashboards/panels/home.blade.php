@extends('layouts.dashboards.app')

@section('dashboard-header')
    <section class="content-header">
      <h1>
        @lang('custom.dashboard')
        <small> @lang('custom.home-desc')</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-dashboard"></i> @lang('custom.home')</a></li>
        <li class="active">@lang('custom.dashboard')</li>
      </ol>
    </section>
@endsection

@section('content')
  <!-- Main content -->
  <!-- Small boxes (Stat box) -->
  <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-4 pd-10-15">
        <div class="box box-home box-home-1">
          <div class="box-header with-border text-center">
            <a href="{{ route('secciones', ['id' => 'organigrama']) }}"
              class="box-title box-title-home">
              <strong>@lang('custom.section-1')</strong>
            </a>
          </div>
          <div class="box-body box-body-home">
            <div class="row">
              <div class="col-md-4">
                <i class="fa fa-sitemap fa-fw fa-1 fa-6x"></i>
              </div>
              <div class="col-md-8">
                <p class="lead pd-15">@lang('custom.section-1-desc')</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-sm-6 col-md-4 pd-10-15">
        <div class="box box-home box-home-2">
          <div class="box-header with-border text-center">
            <a href="{{ route('secciones', ['id' => 'presupuesto']) }}"
              class="box-title box-title-home">
               <strong>@lang('custom.section-2')</strong>
            </a>
          </div>
          <div class="box-body box-body-home">
            <div class="row">
              <div class="col-md-4">
                <i class="fa fa-money fa-fw fa-2 fa-6x"></i>
              </div>
              <div class="col-md-8">
                <p class="lead pd-15">@lang('custom.section-2-desc')</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-sm-6 col-md-4 pd-10-15">
        <div class="box box-home box-home-3">
          <div class="box-header with-border text-center">
            <a href="{{ route('secciones', ['id' => 'compras-y-proveedores']) }}"
              class="box-title box-title-home">
               <strong>@lang('custom.section-3')</strong>
            </a>
          </div>
          <div class="box-body box-body-home">
            <div class="row">
              <div class="col-md-4">
                <i class="fa fa-calculator fa-fw fa-3 fa-6x"></i>
              </div>
              <div class="col-md-8">
                <p class="lead pd-15">@lang('custom.section-3-desc')</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-sm-6 col-md-4 pd-10-15">
        <div class="box box-home box-home-4">
          <div class="box-header with-border text-center">
            <a href="{{ route('secciones', ['id' => 'politicas-sociales']) }}"
              class="box-title box-title-home">
               <strong>@lang('custom.section-4')</strong>
            </a>
          </div>
          <div class="box-body box-body-home">
            <div class="row">
              <div class="col-md-4">
                <i class="fa fa-life-ring fa-fw fa-4 fa-6x"></i>
              </div>
              <div class="col-md-8">
                <p class="lead pd-15">@lang('custom.section-4-desc')</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-sm-6 col-md-4 pd-10-15">
        <div class="box box-home box-home-5">
          <div class="box-header with-border text-center">
            <a href="{{ route('secciones', ['id' => 'salud-y-medioambiente']) }}"
              class="box-title box-title-home">
              <strong>@lang('custom.section-5')</strong>
            </a>
          </div>
          <div class="box-body box-body-home">
            <div class="row">
              <div class="col-md-4">
                <i class="fa fa-heartbeat fa-fw fa-5 fa-6x"></i>
              </div>
              <div class="col-md-8">
                <p class="lead pd-15">@lang('custom.section-5-desc')</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-sm-6 col-md-4 pd-10-15">
        <div class="box box-home box-home-6">
          <div class="box-header with-border text-center">
            <a href="{{ route('secciones', ['id' => 'seguridad-social']) }}"
              class="box-title box-title-home">
               <strong>@lang('custom.section-6')</strong>
            </a>
          </div>
          <div class="box-body box-body-home">
            <div class="row">
              <div class="col-md-4">
                <i class="fa fa-shield fa-fw fa-6 fa-6x"></i>
              </div>
              <div class="col-md-8">
                <p class="lead pd-15">@lang('custom.section-6-desc')</p>
              </div>
            </div>
          </div>
        </div>
      </div>

  </div>
@endsection


@section('footer')
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Bahía Blanca</b> Marzo de 2018
    </div>
    <strong>Trabajo Final de Ingeniería en Sistemas de Información</strong> Departamento de Ciencias e Ingeniería de la Computación, UNS.
  </footer>
@endsection

@section('data-scripts')

@endsection

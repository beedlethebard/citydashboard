@extends('layouts.dashboards.app')

@section('dashboard-header')
    <section class="content-header">
      <h1>
        Secretaría de Modernización y Calidad de Gestión
      </h1>
      <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-dashboard"></i> @lang('custom.home')</a></li>
        <li class="active">Secretaría de Modernización y Calidad de Gestión</li>
      </ol>
    </section>
@endsection

@section('content')
<div class="row">
  <div class="col-lg-4 ">
    <div class="box box-widget widget-user">
      <div class="widget-user-header pd-5" >
        <img src="{{asset('img/muni/modernizacion.png') }}" class="widget-dependency-header" alt="">
      </div>
      <div class="box-footer">
        <div class="row">
          {{-- <div class="col-sm-4 border-right">
            <div class="description-block">
              <h5 class="description-header">3,200</h5>
              <span class="description-text">SALES</span>
            </div>
          </div>
          <div class="col-sm-4 border-right">
            <div class="description-block">
              <h5 class="description-header">13,000</h5>
              <span class="description-text">FOLLOWERS</span>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="description-block">
              <h5 class="description-header">35</h5>
              <span class="description-text">PRODUCTS</span>
            </div>
          </div> --}}
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('footer')

@endsection

@section('data-scripts')

@endsection

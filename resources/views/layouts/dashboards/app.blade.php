<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

    @include('layouts.dashboards.partials.head')

    <body class="hold-transition skin-black sidebar-mini sidebar-collapse">
        <div id="app" class="wrapper">

        @include('layouts.dashboards.partials.header')
        @include('layouts.dashboards.partials.sidebar')

        <main class="content-wrapper">
            @yield('dashboard-header')

            <section class="content">
              @yield('content')
            </section>
        </main>

        {{-- @include('layouts.partials.control-sidebar') --}}
        @yield('footer')
        </div>

        @include('layouts.dashboards.partials.scripts')
        @yield('data-scripts')
    </body>
</html>

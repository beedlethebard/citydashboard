@extends('layouts.landing.app')

@section('main')
<div class="wrapper" id="top-image">
  <div class="header header-filter">
    <div class="container">
      <div class="row">
				<div class="col-md-6">
					<h1 class="title">
            @lang('custom.landing-title')
          </h1>
          <h4 style="font-weight: 400">
            @lang('custom.landing-desc')
          </h4>
          <br/>
          <a href="http://datos.bahiablanca.gob.ar/home"
              target="_blank"
              class="btn btn-danger btn-raised btn-lg">
					   <i class="fa fa-database fa-fw"></i> @lang('custom.landing-button')
				  </a>
				</div>
      </div>
    </div>
  </div>
</div>
@endsection

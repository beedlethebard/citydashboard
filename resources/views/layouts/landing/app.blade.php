<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    
    @include('layouts.landing.partials.head')

    <body class="landing-page" >

        @include('layouts.landing.partials.header')

        @yield('main')

        @include('layouts.landing.partials.scripts')
    </body>
</html>

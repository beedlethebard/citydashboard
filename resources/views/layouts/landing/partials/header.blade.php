<!-- Fixed navbar -->
<nav class="navbar navbar-transparent navbar-absolute">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="https://www.uns.edu.ar/" target="_blank">UNS</a>
      <a class="navbar-brand" href="https://cs.uns.edu.ar/home/" target="_blank">DCIC</a>
      <a class="navbar-brand" href="https://lissi.cs.uns.edu.ar/" target="_blank">LISSI</a>
      <a class="navbar-brand" href="http://www.cic.gba.gob.ar/" target="_blank">CIC</a>
    </div>
    @if (Route::has('login'))
    <div id="navbar" class="navbar-collapse collapse pull-right">
      <ul class="nav navbar-nav">
          @auth
            <li class="active"><a href="{{ url('/home') }}">Home</a></li>
          @else
            @if(!Request::is('/'))
                <li><a href="{{ url('/') }}"><i class="fa fa-line-chart fa-fw"></i> City Dashboard</a></li>
            @endif
            @if(!Request::is('login'))
                <li><a href="{{ route('login') }}"><i class="fa fa-sign-in fa-fw"></i> @lang('custom.login')</a></li>
            @endif
            @if(!Request::is('register'))
                <li><a href="{{ route('register') }}"><i class="fa fa-user-circle fa-fw"></i> @lang('custom.register')</a></li>
            @endif
        @endauth
      </ul>
    </div><!--/.nav-collapse -->
    @endif
  </div>
</nav>

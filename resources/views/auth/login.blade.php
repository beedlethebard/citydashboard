@extends('auth.form')

@section('form')
<div class="wrapper">
  <div class="header header-filter" id="top-image">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
          <div class="card card-signup">

            <form method="POST" action="{{ route('login') }}">
              @csrf

              <div id="form-title" class="header header-primary text-center">
                <h4>@lang('custom.siginsession')</h4>
              </div>
              <div class="content">
                <div class="input-group">
                  <span class="input-group-addon">
                    <i class="fa fa-user fa-fw"></i>
                  </span>
                  <input id="email" type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Direccion de email" required autofocus>

                  @if ($errors->has('email'))
                    <span class="help-block">
                      <strong>{{ $errors->first('email')}}</strong>
                    </span>
                  @endif
                </div>

                <div class="input-group">
                  <span class="input-group-addon">
                    <i class="fa fa-lock fa-fw"></i>
                  </span>
                  <input id="password" type="password" name="password" class="form-control" placeholder="Contraseña" required>

                  @if ($errors->has('password'))
                    <span class="help-block">
                      <strong>{{ $errors->first('password')}}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="footer text-center">
                <button type="submit" class="btn btn-primary" style="font-size: 15px;">@lang('custom.buttonsign')</button>
              </div>
            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

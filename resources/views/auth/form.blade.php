<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

    @include('layouts.landing.partials.head')

    <body class="signup-page" >

        @include('layouts.landing.partials.header')

        @yield('form')

        @include('layouts.landing.partials.scripts')
    </body>
</html>

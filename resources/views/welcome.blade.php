<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'City Dashboard') }}</title>

        @include('layouts.partials.head')
        <!-- Styles -->
        <style>
            html, body {
                /* background-image: linear-gradient(
                                      rgba(0, 0, 0, 0.8),
                                      rgba(0, 0, 0, 0.2)
                                  ), url("../img/bg.jpg");
                background-size: cover; */
                /* background-color: #cccccc; */
                color: #fff;
                font-family: "Roboto", sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            #top-image {
                background:linear-gradient(
                                      rgba(0, 0, 0, 0.6),
                                      rgba(0, 0, 0, 0.6)
                                  ), url("../img/bg.jpg") -25px -50px;
                position:fixed ;
                top:0;
                width:100%;
                z-index:0;
                height:100%;
                background-size: calc(100% + 150px);
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .dark-bg {
                background: rgba(0, 0, 0, 0.6);
                color: #fff;
                display: inline;
                padding: 0.5rem;

                -webkit-box-decoration-break: clone;
                box-decoration-break: clone;
            }

            .links > a {
                /* color: #636b6f; */
                color: #fff;
                padding: 5px 25px;
                font-size: 15px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
            .links > a:hover {
                background: rgba(0, 0, 0, 0.8);
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .m-t-20 {
                margin-top: 20px;
            }
        </style>
    </head>
    <body id="top-image">
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        @if(!Request::is('login'))
                            <a href="{{ route('login') }}" class="dark-bg"><i class="fa fa-sign-in"></i> @lang('custom.login')</a>
                        @endif
                        @if(!Request::is('register'))
                            <a href="{{ route('register') }}" class="dark-bg"><i class="fa fa-user-circle"></i> @lang('custom.register')</a>
                        @endif
                    @endauth
                </div>
            @endif

            @yield('content')
        </div>
        @include('layouts.partials.scripts')
    </body>
</html>

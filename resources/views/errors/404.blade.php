@extends('layouts.landing.app')

@section('main')
<div class="wrapper" id="top-image">
    <div class="header header-filter">
        <div class="container" style="display: table; height:200px;">
            <div class="row" style="display: table-cell; vertical-align: middle;">
				<div class="col-md-13 text-center">
					<h2 class="title" >Perdón, la página que estás buscando no se pudo encontrar.</h2>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection

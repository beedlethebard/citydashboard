<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.landing.sections.main');
});

Auth::routes();

Route::get('home', 'HomeController@index')->name('home');
Route::get('municipio', 'MunicipioController@index')->name('municipio');
Route::get('intendencia', 'IntendenciaController@index')->name('intendencia');
Route::get('concejo', 'ConcejoController@index')->name('concejo');

// Route::get('asesoria-letrada', 'AsesoriaLetradaController@index')->name('asesoria-letrada');
// Route::get('gestion-ambiental', 'GestionAmbientalController@index')->name('gestion-ambiental');
// Route::get('gobierno', 'GobiernoController@index')->name('gobierno');
// Route::get('hacienda', 'HaciendaController@index')->name('hacienda');
// Route::get('infraestructura', 'InfraestructuraController@index')->name('infraestructura');
// Route::get('innovacion', 'InnovacionController@index')->name('innovacion');
// Route::get('modernizacion', 'ModernizacionController@index')->name('modernizacion');
// Route::get('politicas-sociales', 'PoliticasSocialesController@index')->name('politicas-sociales');
// Route::get('salud', 'SaludController@index')->name('salud');
// Route::get('seguridad', 'SeguridadController@index')->name('seguridad');
// Route::get('cultura', 'CulturaController@index')->name('cultura');

Route::get('secretarias/{id}', 'SecretariasController@index')->name('secretarias');

Route::get('secciones/{id}', 'SeccionesController@index')->name('secciones');

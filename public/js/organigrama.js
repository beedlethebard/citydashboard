organigrama = ﻿{ //el HCD se lleva el 1,9%
  "Oficina": "Intendencia",
  "Responsable": "Gay, Héctor Norberto",
  "Cargo": "Intendente",
  "children": [
    {
      "Oficina": "Secretaría Privada", //Gastos: suma de gastos de intendencia/cantHojas (http://www.bahia.gob.ar/privada/datosabiertos_presupuesto/)
      "Responsable": "Marisco, Tomás Adrián",
      "Cargo": "Secretario",
      "Porcentaje": 1.11,
      "children": [
        {
          "Oficina": "Subsecretaría de Prensa",
          "Responsable": "Romera, Pablo Mariano",
          "Cargo": "Subsecretario",
          "size": 8183902,
        },
        {
          "Oficina": "Subsecretaría de Comunicación",
          "Responsable": "Rossi, Carlos Alberto",
          "Cargo": "Subsecretario",
          "size": 8183902
        },
        {
          "Oficina": "Dirección de Ceremonial y Protocolo",
          "Responsable": "Corinaldesi, Silvia Beatriz",
          "Cargo": "Director",
          "size": 8183902
        },
        {
          "Oficina": "Mesa de Informes",
          "Responsable": "Schefer, Paula",
          "Cargo": "Responsable",
          "size": 8183902
        }
      ]
    },
    {
      "Oficina": "Secretaría de Asesoría Letrada",
      "Responsable": "Mahón, Karina Mariel",
      "Cargo": "Secretario",
      "Porcentaje": 0.48,
      "children": [
        {
          "Oficina": "Subsecretaría Jurídica",
          "Responsable": "Jordán, Silvia Cristina",
          "Cargo": "Subsecretario",
          "children": [
            {
              "Oficina": "División Área de Dictámenes",
              "Responsable": "López, Manuel C.",
              "Cargo": "Jefe de División",
              "size": 7120387,
            }
          ]
        },
        {
          "Oficina": "División Jefe de Despacho",
          "Responsable": "...",
          "Cargo": "+++",
          "size": 7120387,
        }
      ]
    },
    {
      "Oficina": "Secretaría de Innovación Tecnológica y Desarrollo Creativo",
      "Responsable": "Riva, Diego Ceferino",
      "Cargo": "Secretario",
      "Porcentaje": 0.33,
      "children": [
        {
          "Oficina": "Director General de Innovación y Desarrollo Creativo",
          "Responsable": "Kobalenko, Sergio Ariel",
          "Cargo": "Director General",
          "size": 2170741,
        },
        {
          "Oficina": "Director de Innovación",
          "Responsable": "Luján, Javier Joel",
          "Cargo": "Director",
          "size": 2170741,
        },
        {
          "Oficina": "Director de Enprendedurismo",
          "Responsable": "Montero, Diego",
          "Cargo": "Director",
          "size": 2170741,
        },
        {
          "Oficina": "División Jefe de Despacho",
          "Responsable": "...",
          "Cargo": "+++",
          "size": 2170741,
        },
      ]
    },
    {
      "Oficina": "Secretaría de Hacienda y Desarrollo Económico",
      "Responsable": "Esandi, Juan Ignacio",
      "Cargo": "Secretario",
      "Porcentaje": 5.43,
      "children": [
        {
          "Oficina": "Subsecretaría de Desarrollo Económico",
          "Responsable": "Garat, Paulo José",
          "Cargo": "Subsecretario",
          "children":[
            {
              "Oficina": "Agencia de Inversiones y Comercio Exterior",
              "Responsable": "Stegmann, Pablo",
              "Cargo": "Jefe de División",
              "size": 6170517,
            }
          ]
        },
        {
          "Oficina": "Dirección de Rentas",
          "Responsable": "López, Mario",
          "Cargo": "Secretario",
          "children": [
            {
              "Oficina": "Departamento de Ingresos y Política Fiscal",
              "Responsable": "-, -",
              "Cargo": "Director Adjunto",
              "size": 6170517,
            },
            {
              "Oficina": "Departamento de Inspecciones",
              "Responsable": "Ramos, Orlando",
              "Cargo": "Jefe de Departamento",
              "size": 6170517,
            },
            {
              "Oficina": "Departamento de Recaudación",
              "Responsable": "Iriart, Diana",
              "Cargo": "Jefe de Departamento",
              "size": 6170517,
            },
            {
              "Oficina": "Departamento de Habilitaciones",
              "Responsable": "Tumine, María Margarita",
              "Cargo": "Jefe de Departamento",
              "size": 6170517,
            },
            {
              "Oficina": "División de Inspecciones 1",
              "Responsable": "Bettucci, Santiago Carlos",
              "Cargo": "Jefe de División",
              "size": 6170517,
            },
            {
              "Oficina": "División de Inspecciones 2",
              "Responsable": "Denis, Carlos José",
              "Cargo": "Jefe de División",
              "size": 6170517,
            },
            {
              "Oficina": "División Jefe de Despacho",
              "Responsable": "...",
              "Cargo": "+++",
              "size": 6170517,
            },
            {
              "Oficina": "División de Recaudación",
              "Responsable": "Pérez Wallace, Guillermina",
              "Cargo": "Jefe de División",
              "size": 6170517,
            },
            {
              "Oficina": "División de Habilitaciones",
              "Responsable": "Romanelli, Cristina",
              "Cargo": "Jefe de División",
              "size": 6170517,
            },
            {
              "Oficina": "Departamento de Consorcios y Obras Básicas",
              "Responsable": "Henales, María Angélica",
              "Cargo": "Jefe de División",
              "size": 6170517,
            },

          ]
        },
        {
          "Oficina": "Dirección Municipal de Estadísticas",
          "Responsable": "Delbianco, Fernando Andrés",
          "Cargo": "Director General",
          "size": 6170517,
        },
        {
          "Oficina": "División Jefe de Despacho",
          "Responsable": "...",
          "Cargo": "+++",
          "size": 6170517,
        },
        {
          "Oficina": "Departamento de Contaduría",
          "Responsable": "Clark, Marcela",
          "Cargo": "Contador General",
          "children": [
            {
              "Oficina": "Subcontaduría",
              "Responsable": "Brozzi, Leandro",
              "Cargo": "Subcontador",
              "size": 6170517,
            },
            {
              "Oficina": "Jefa de Departamento",
              "Responsable": "Giovaniello, Liliana",
              "Cargo": "Jefe de Departamento",
              "size": 6170517,
            },
            {
              "Oficina": "Departamento de Registro Patrimonial",
              "Responsable": "Rodríguez Peraita, María Inés",
              "Cargo": "Jefe de Departamento",
              "size": 6170517,
            },
            {
              "Oficina": "División Jefa de Despacho",
              "Responsable": "...",
              "Cargo": "+++",
              "size": 6170517,
            }
          ]
        },
        {
          "Oficina": "Departamento de Compras",
          "Responsable": "Trellini, Mariano N.",
          "Cargo": "Jefe de Compras",
          "size": 6170517,
        },
        {
          "Oficina": "Departamento de Tesorería",
          "Responsable": "Panis, Leandro",
          "Cargo": "Tesorero",
          "size": 6170517,
        },
        {
          "Oficina": "Subdirección de Presupuesto y Planificación",
          "Responsable": "Giagante, Gabriela",
          "Cargo": "Subdirector",
          "children": [
            {
              "Oficina": "Departamento de Planificación y Crédito Público",
              "Responsable": "Larrondo, Ignacio",
              "Cargo": "Jefe de Departamento",
              "size": 6170517,
            },
            {
              "Oficina": "División de Presupuesto y Planificación",
              "Responsable": "Hernández, Walter",
              "Cargo": "Jefe de División",
              "size": 6170517,
            },
          ]
        },
        {
          "Oficina": "Subdirección de Capital Humano",
          "Responsable": "Lucanera, Natalia",
          "Cargo": "Subdirector",
          "children": [
            {
              "Oficina": "Departamento de Administración de Personal",
              "Responsable": "Sensini, Mirta",
              "Cargo": "Jefe de Departamento",
              "size": 6170517,
            },
            {
              "Oficina": "Departamento de Reconocimientos Médicos",
              "Responsable": "Banderet, Rubén",
              "Cargo": "Jefe de Departamento",
              "size": 6170517,
            },
            {
              "Oficina": "Departamento de Recursos Humanos",
              "Responsable": "Ducasu, Claudia",
              "Cargo": "Jefe de Departamento",
              "size": 6170517,
            },
            {
              "Oficina": "Seguridad e Higiene del Trabajo",
              "Responsable": "-, -",
              "Cargo": "Coordinador",
              "size": 6170517,
            },
            {
              "Oficina": "División Jubilaciones y Pensiones",
              "Responsable": "Gimeno, María Inés",
              "Cargo": "Jefe de División",
              "size": 6170517,
            },
          ]
        },
      ]
    },
    {
      "Oficina": "Secretaría de Gobierno",
      "Responsable": "Pierdominici, Fabio Gabriel",
      "Cargo": "Secretario",
      "Porcentaje": 7.66,
      "children": [
        {
          "Oficina": "Dirección Mercado Municipal",
          "Responsable": "Natol, Hugo",
          "Cargo": "Director",
          "size": 18871126,
        },
        {
          "Oficina": "Dirección de Gobierno",
          "Responsable": "Lorea, María Belén",
          "Cargo": "Director",
          "children": [
            {
              "Oficina": "Subdirección de Gobierno",
              "Responsable": "Tassi, María de los Ángeles",
              "Cargo": "Subdirector",
              "size": 18871126,
            }
          ]
        },
        {
          "Oficina": "Dirección Terminal de Ómbibus 'San Francisco de Asis'",
          "Responsable": "López, César",
          "Cargo": "Director",
          "size": 18871126,
        },
        {
          "Oficina": "Dirección General de Tránsito y Transporte",
          "Responsable": "Frapiccini, Ramiro",
          "Cargo": "Director",
          "children": [
            {
              "Oficina": "Departamento Administrativo de Tránsito",
              "Responsable": "Chavero, Mónica Ethel",
              "Cargo": "Jefe de Departamento",
              "size": 18871126,
            },
            {
              "Oficina": "División Transporte",
              "Responsable": "Colantonio, Raúl Ernesto",
              "Cargo": "Jefe de División",
              "size": 18871126,
            },
            {
              "Oficina": "Agencia Municipal de Seguridad Vial",
              "Responsable": "Pezzella, Francisco Gabriel",
              "Cargo": "Jefe de División",
              "size": 18871126,
            },
          ]
        },
        {
          "Oficina": "Dirección General de Fiscalización",
          "Responsable": "Montanaro, Jóse Luis",
          "Cargo": "Director",
          "size": 18871126,
        },
        {
          "Oficina": "División Mesa de Entradas y Archivo",
          "Responsable": "Ravani, Guillermo Carlos",
          "Cargo": "Jefe de División",
          "size": 18871126,
        },
        {
          "Oficina": "División Control de Tránsito Urbano",
          "Responsable": "Plaide, Mauro",
          "Cargo": "Jefe de División",
          "size": 18871126,
        },
        {
          "Oficina": "Tribunal de Faltas Nº 1",
          "Responsable": "Germani, Ricardo Alberto",
          "Cargo": "Juez",
          "size": 18871126,
        },
        {
          "Oficina": "Tribunal de Faltas Nº 2",
          "Responsable": "Nardi, Gabriel Alberto",
          "Cargo": "Juez",
          "size": 18871126,
        },
        {
          "Oficina": "Coloso Cultural",
          "Responsable": "Borrego, Yanina",
          "Cargo": "Coordinador",
          "size": 18871126,
        }
      ]
    },
    {
      "Oficina": "Instituto Cultural Municipal",
      "Responsable": "Margo, Héctor Ricardo",
      "Cargo": "Secretario",
      "Gasto": 50412286,
      "Porcentaje": 1.58,
      "children": [
        {
          "Oficina": "Dirección de Teatro Municipal",
          "Responsable": "Agesta, Enrique",
          "Cargo": "Director",
          "children": [
            {
              "Oficina": "Sala Payró",
              "Responsable": "-, -",
              "Cargo": "Coordinador",
              "size": 2124933,
            }
          ]
        },
        {
          "Oficina": "Dirección de Turismo",
          "Responsable": "Arocena, Julia",
          "Cargo": "Jefe de Departamento",
          "children": [
            {
              "Oficina": "Oficina de Turismo de La Términal de Ómnibus",
              "Responsable": "-, -",
              "Cargo": "Coordinador",
              "size": 2124933,
            },
            {
              "Oficina": "Peatonal Drago",
              "Responsable": "-, -",
              "Cargo": "Coordinador",
              "size": 2124933,
            }
          ]
        },
        {
          "Oficina": "División Jefe de Despacho",
          "Responsable": "...",
          "Cargo": "+++",
          "size": 2124933,
        },
        {
          "Oficina": "Consejo Municipal De Bibliotecas Populares",
          "Responsable": "Ramírez, Teresa",
          "Cargo": "Coordinador",
          "size": 2124933,
        },
        {
          "Oficina": "Talleres Artísticos Municipales",
          "Responsable": "-, -",
          "Cargo": "Coordinador",
          "size": 2124933,
        },
        {
          "Oficina": "Coordinación de Recursos Artísticos",
          "Responsable": "Baridón, Néstor",
          "Cargo": "Coordinador",
          "children":[
            {
              "Oficina": "Coral Cabildo Coordinadora",
              "Responsable": "-, -",
              "Cargo": "Coordinador",
              "size": 2124933,
            },
            {
              "Oficina": "Coro de Niños de la Ciudad",
              "Responsable": "-, -",
              "Cargo": "Coordinador",
              "size": 2124933,
            },
            {
              "Oficina": "Coro de Jóvenes de la Ciudad",
              "Responsable": "-, -",
              "Cargo": "Coordinador",
              "size": 2124933,
            },
            {
              "Oficina": "Coro de Adultos de la Ciudad",
              "Responsable": "-, -",
              "Cargo": "Coordinador",
              "size": 2124933,
            },
            {
              "Oficina": "Comunicación y Diseño",
              "Responsable": "-, -",
              "Cargo": "Coordinador",
              "size": 2124933,
            },
            {
              "Oficina": "Banda Juvenil Municipal",
              "Responsable": "-, -",
              "Cargo": "Coordinador",
              "size": 2124933,
            },
            {
              "Oficina": "Comedia Musical",
              "Responsable": "-, -",
              "Cargo": "Coordinador",
              "size": 2124933,
            },
          ]
        },
        {
          "Oficina": "Centro Municipal de Estudios Folklóricos",
          "Responsable": "-, -",
          "Cargo": "Coordinador",
          "size": 2124933,
        },
        {
          "Oficina": "Centro de Gestión Cultural islas Malvinas",
          "Responsable": "-, -",
          "Cargo": "Coordinador",
          "size": 2124933,
        },
        {
          "Oficina": "Coordinación de Museos",
          "Responsable": "Fuentes, Marina Elena",
          "Cargo": "Coordinador",
          "children": [
            {
              "Oficina": "Museo del Puerto",
              "Responsable": "Beier, Leandro",
              "Cargo": "Director",
              "size": 2124933,
            },
            {
              "Oficina": "Museo Histórico y Archivo Municipal",
              "Responsable": "Dozo, Ana",
              "Cargo": "Director",
              "children": [
                {
                  "Oficina": "Museo Fortín Cuatreros",
                  "Responsable": "-, -",
                  "Cargo": "Coordinador",
                  "size": 2124933,
                }
              ]
            },
            {
              "Oficina": "Museo de Ciencias Naturales",
              "Responsable": "Dascanio, Liliana",
              "Cargo": "Director",
              "size": 2124933,
            },
            {
              "Oficina": "Museo del Deporte",
              "Responsable": "Velázquez, Claudio",
              "Cargo": "Director",
              "size": 2124933,
            },
            {
              "Oficina": "Museo de Arte Contemporáneo y Bellas Artes",
              "Responsable": "Miconi, Cecilia",
              "Cargo": "Director",
              "size": 2124933,
            },
            {
              "Oficina": "FerroWhite Museo/Taller",
              "Responsable": "Testoni, Nicolás",
              "Cargo": "Director",
              "size": 2124933,
            },
          ]
        },
        {
          "Oficina": "División Áreas Verdes",
          "Responsable": "Dicek, Norman",
          "Cargo": "Coordinador",
          "size": 2124933,
        }
      ]
    },
    {
      "Oficina": "Secretaría de Infraestructura",
      "Responsable": "Manrique, Guillermo Osvaldo",
      "Cargo": "Secretario",
      "Gasto": 652679321,
      "Porcentaje": 35.45,
      "children": [
        {
          "Oficina": "Subsecretaría de Planificación y Desarrollo Urbano",
          "Responsable": "Pites, Luis Miguel Vicente",
          "Cargo": "Subsecretario",
          "children": [
            {
              "Oficina": "Dirección de Planeamiento Urbano",
              "Responsable": "De Carolis, María Martha",
              "Cargo": "Director",
              "children": [
                {
                  "Oficina": "Departamento de Parques Municipales",
                  "Responsable": "Gutierrez, Erica Noelia",
                  "Cargo": "Jefe de Departamento",
                  "size": 33402926,
                },
                {
                  "Oficina": "Departamento de Obras Particulares",
                  "Responsable": "Villavilla, Fernando",
                  "Cargo": "Jefe de Departamento",
                  "size": 33402926,
                },
                {
                  "Oficina": "Departamento Proyectos Urbanos",
                  "Responsable": "Colina, Iván",
                  "Cargo": "Jefe de División",
                  "size": 33402926,
                },
              ]
            },
            {
              "Oficina": "Departamento Catastro",
              "Responsable": "De Aduriz, Ana",
              "Cargo": "Jefe de Departamento",
              "children": [
                {
                  "Oficina": "Departamento Territorial",
                  "Responsable": "Urquiza, Cristina",
                  "Cargo": "Jefe de División",
                  "size": 33402926,
                },
                {
                  "Oficina": "División Central Territorial de Datos",
                  "Responsable": "Forgia, Pablo",
                  "Cargo": "Jefe de División",
                  "size": 33402926,
                },
              ]
            },
            {
              "Oficina": "Subdirección de Tierras",
              "Responsable": "Retta, Estela",
              "Cargo": "Subdirector",
              "size": 33402926,
            },
          ]
        },
        {
          "Oficina": "Subsecretaría de Proyectos y Obras",
          "Responsable": "Meneses, Alejandro Claudio",
          "Cargo": "Subsecretario",
          "children": [
            {
              "Oficina": "Departamento de Electricidad y Mecánica",
              "Responsable": "Dotti, Daniel",
              "Cargo": "Jefe de Departamento",
              "size": 33402926,
            },
            {
              "Oficina": "Departamento de Proyecto y Obras",
              "Responsable": "González, José María",
              "Cargo": "Jefe de Departamento",
              "size": 33402926,
            },
            {
              "Oficina": "Departamento de Vialidad",
              "Responsable": "Hughes, Ricardo",
              "Cargo": "Jefe de Departamento",
              "size": 33402926,
            },
            {
              "Oficina": "Departamento de Vivienda",
              "Responsable": "Pistarelli, Juan Carlos",
              "Cargo": "Jefe de Departamento",
              "size": 33402926,
            }
          ]
        },
        {
          "Oficina": "Subsecretaría de Mantenimiento",
          "Responsable": "Carbone, Daniel",
          "Cargo": "Subsecretario",
          "children": [
            {
              "Oficina": "Departamento de Talleres y Maestranza",
              "Responsable": "Romagnoli, Ramón",
              "Cargo": "Jefe de Departamento",
              "size": 33402926,
            },
            {
              "Oficina": "Departamento Mantenenimiento Obras Civiles",
              "Responsable": "Anrique, Javier",
              "Cargo": "Jefe de Departamento",
              "size": 33402926,
            },
            {
              "Oficina": "División Parque Automotor",
              "Responsable": "Asenci, Adrián",
              "Cargo": "Jefe de División",
              "size": 33402926,
            },
            {
              "Oficina": "División Mayordomía",
              "Responsable": "Navarro, Oscar H",
              "Cargo": "Jefe de División",
              "size": 33402926,
            },
            {
              "Oficina": "Delegación Cabildo",
              "Responsable": "Bartel, Néstor Luis",
              "Cargo": "Delegado",
              "size": 33402926,
            },
            {
              "Oficina": "Delegación General Daniel Cerri",
              "Responsable": "Sangre, Emilio Alberto",
              "Cargo": "Delegado",
              "size": 33402926,
            },
            {
              "Oficina": "Delegación Ingeniero White",
              "Responsable": "Pignatelli, Marisa",
              "Cargo": "Delegado",
              "size": 33402926,
            },
            {
              "Oficina": "Delegación Las Villas",
              "Responsable": "Aguilera, Pablo Andrés",
              "Cargo": "Delegado",
              "size": 33402926,
            },
            {
              "Oficina": "Delegación Villa Rosas",
              "Responsable": "Calvismonte, Alberto Fabián",
              "Cargo": "Delegado",
              "size": 33402926,
            },
            {
              "Oficina": "Delegación Villa Harding Green",
              "Responsable": "Schwerdt, Eduardo Néstor",
              "Cargo": "Delegado",
              "size": 33402926,
            },
            {
              "Oficina": "Delegación Norte",
              "Responsable": "Salvarezza, Adrián Enrique",
              "Cargo": "Delegado",
              "size": 33402926,
            },
            {
              "Oficina": "Delegación Noroeste",
              "Responsable": "Campos, Juan Manuel",
              "Cargo": "Delegado",
              "size": 33402926,
            },
            {
              "Oficina": "Delegación Centro",
              "Responsable": "Pastor, Marcelo Fabián",
              "Cargo": "Delegado",
              "size": 33402926,
            },
            {
              "Oficina": "División de Inspecciones de Tránsito",
              "Responsable": "Haag, Claudio",
              "Cargo": "Coordinador",
              "size": 33402926,
            },
            {
              "Oficina": "Dirección de Logística y Limpieza Urbana",
              "Responsable": "Bayer, Javier",
              "Cargo": "Subdirector",
              "size": 33402926,
            },
          ]
        },
        {
          "Oficina": "Dirección de Áreas Públicas",
          "Responsable": "Díaz Martínez, María Elena",
          "Cargo": "Director",
          "children": [
            {
              "Oficina": "Departamento Mantenimiento Alumbrado Público",
              "Responsable": "González, Andrea",
              "Cargo": "Jefe de Departamento",
              "size": 33402926,
            },

          ]
        },
        {
          "Oficina": "Dirección de Control de Gestión",
          "Responsable": "Mishevitch, Pablo Emilio",
          "Cargo": "Director",
          "size": 33402926,
        },
        {
          "Oficina": "División Jefe de Despacho",
          "Responsable": "...",
          "Cargo": "+++",
          "size": 33402926,
        }
      ]
    },
    {
      "Oficina": "Secretaría de Políticas Sociales",
      "Responsable": "Monardez, María Soledad",
      "Cargo": "Secretario",
      "Gasto": 277826904,
      "Porcentaje": 9.84,
      "children": [
        {
          "Oficina": "Subsecretaría de Acción Social",
          "Responsable": "Caspe, Ignacio Fermín",
          "Cargo": "Subsecretario",
          "children": [
            {
              "Oficina": "Dirección de Participación Ciudadana",
              "Responsable": "Horvath, María Emilia",
              "Cargo": "Director",
              "children": [
                {
                  "Oficina": "Colonia Agustín de Arrieta",
                  "Responsable": "-, -",
                  "Cargo": "Coordinador",
                  "size": 5934186,
                }
              ]
            },
            {
              "Oficina": "Dirección de Juventud",
              "Responsable": "Bustelo, María Florencia",
              "Cargo": "Director",
              "size": 5934186,
            },
            {
              "Oficina": "Dirección de Empleo",
              "Responsable": "Cardozo, César",
              "Cargo": "Director",
              "size": 5934186,
            },
            {
              "Oficina": "Dirección Servicio Social",
              "Responsable": "Canosa Contín, María Cecilia",
              "Cargo": "Director",
              "children": [
                {
                  "Oficina": "División Servicio Social",
                  "Responsable": "Ojeda, Fabiana del Carmen",
                  "Cargo": "Jefe de División",
                  "size": 5934186,
                },
                {
                  "Oficina": "División Acción Comunitaria",
                  "Responsable": "Chauvie, Claudia",
                  "Cargo": "Jefe de División",
                  "children": [
                    {
                      "Oficina": "Depósito de Acción Comunitaria",
                      "Responsable": "De Uribe, Alberto",
                      "Cargo": "Responsable",
                      "size": 5934186,
                    }
                  ]
                }
              ]
            },
            {
              "Oficina": "División Plan Más vida",
              "Responsable": "Cátala, Norma",
              "Cargo": "Jefe de División",
              "size": 5934186,
            },
            {
              "Oficina": "Oficina de Personas Jurídicas",
              "Responsable": "Assad, Nicolás",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Dirección Adultos Mayores",
              "Responsable": "-, -",
              "Cargo": "Coordinador",
              "size": 5934186,
            },
          ]
        },
        {
          "Oficina": "Subsecretaría de Deportes",
          "Responsable": "Stortoni, Bernardo",
          "Cargo": "Subsecretario",
          "children": [
            {
              "Oficina": "Balneario Maldonado",
              "Responsable": "-, -",
              "Cargo": "Coordinador",
              "size": 5934186,
            }
          ]
        },
        {
          "Oficina": "Subsecretaría de Promoción y Protección de Derechos",
          "Responsable": "Tamborindiguy, Letizia Karenina",
          "Cargo": "Subsecretario",
          "children": [
            {
              "Oficina": "Dirección General De Integración y Fortalecimiento de Derechos Humanos",
              "Responsable": "-, -",
              "Cargo": "Director General",
              "size": 5934186,
            },
            {
              "Oficina": "Dirección de Políticas de Género",
              "Responsable": "Andenoche, Víctor Martín",
              "Cargo": "Director",
              "size": 5934186,
            },
            {
              "Oficina": "Dirección de Servicio Local",
              "Responsable": "Maricic, Gimena",
              "Cargo": "Director",
              "size": 5934186,
            },
            {
              "Oficina": "Dirección de Inclusión y Accesibilidad",
              "Responsable": "Pendino, María Lucía",
              "Cargo": "Director",
              "size": 5934186,
            },
            {
              "Oficina": "Dirección Hogares de Prevención y Protección",
              "Responsable": "Roth, Germán",
              "Cargo": "Director",
              "size": 5934186,
            },
            {
              "Oficina": "Dirección Fortalecimiento Humano",
              "Responsable": "Sgalla, Silvia",
              "Cargo": "Director",
              "size": 5934186,
            },
            {
              "Oficina": "División de Accesibilidad e Inclusión",
              "Responsable": "Chavero, Marcela",
              "Cargo": "Jefe de División",
              "size": 5934186,
            },
            {
              "Oficina": "Hogar del Adolescente",
              "Responsable": "Jaramillo, Víctor",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Instituto José Luis cantilo",
              "Responsable": "Álvarez, María José",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Casa del Niño Villa Rosas",
              "Responsable": "Faath, Olga",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Liliana Abia de Ceci (Ex Casa del Niño de Ingeniero White)",
              "Responsable": "Giampaoletti, Graciela",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Centro de Día Sueño de Barrilete",
              "Responsable": "Fantino, Elmo",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Pequeños Hogares - Hogar Sustituto",
              "Responsable": "Lertóla, Elizabeth",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Centro de Capacitación y Promoción Comunitaria de Villa Miramar",
              "Responsable": "Angelini, Mónica",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Mamas Cuidadoras",
              "Responsable": "Sarden, Graciela",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Proyecto Apicultura",
              "Responsable": "Rouleder, María Graciela",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Sistemas Amas Externas",
              "Responsable": "Vercelino, Rául",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Hogar Refugio de Violencia Familiar",
              "Responsable": "Fuidio, Mónica",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Hogar de Niños y Niñas Peumayen",
              "Responsable": "Veiga, Luz",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Casa de Abrigo II",
              "Responsable": "Pelendier, Silvia",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Programa Psicoterapeútico Caballo de Troya",
              "Responsable": "Colisnechenko, Sonia",
              "Cargo": "Coordinador",
              "size": 5934186,
            },
            {
              "Oficina": "Coordinación Programa ENVION",
              "Responsable": "D'Alberti, Lucía",
              "Cargo": "Coordinador",
              "children": [
                {
                  "Oficina": "Programa ENVION Stella Maris",
                  "Responsable": "Pasquaré, Luciana",
                  "Cargo": "Coordinador",
                  "size": 5934186,
                },
                {
                  "Oficina": "Programa ENVION Spurr",
                  "Responsable": "Vásquez, Santiago",
                  "Cargo": "Coordinador",
                  "size": 5934186,
                },
                {
                  "Oficina": "Programa ENVION Caracol",
                  "Responsable": "Vara, María José",
                  "Cargo": "Coordinador",
                  "size": 5934186,
                },
                {
                  "Oficina": "Programa ENVION Saladero",
                  "Responsable": "Cascallar, Jorge",
                  "Cargo": "Coordinador",
                  "size": 5934186,
                },
                {
                  "Oficina": "Programa ENVION Villa Harding Green",
                  "Responsable": "Lunazzi, Natalia",
                  "Cargo": "Coordinador",
                  "size": 5934186,
                },
                {
                  "Oficina": "Programa ENVION Norte",
                  "Responsable": "-, -",
                  "Cargo": "Responsable",
                  "size": 5934186,
                }
              ]
            },
          ]
        },
        {
          "Oficina": "Subsecretaría de Formación y Promoción Educativa",
          "Responsable": "Llanca Rosello, Morena",
          "Cargo": "Subsecretario",
          "children": [
            {
              "Oficina": "Dirección General Planeamiento y Control de Gestión",
              "Responsable": "Kraser, Nelson José",
              "Cargo": "Director General",
              "children": [
                {
                  "Oficina": "Escuela Municipal de Capacitación Laboral San Roque",
                  "Responsable": "-, -",
                  "Cargo": "Coordinador",
                  "size": 5934186,
                }
              ]
            },
            {
              "Oficina": "Dirección General Programas y Proyectos de Formación y Capacitación",
              "Responsable": "Uset, Estebán Fernando",
              "Cargo": "Director General",
              "size": 5934186,
            },
            {
              "Oficina": "Dirección General Gestión Educativa y Coordinación Pedagógica Municipal",
              "Responsable": "Ayala, Romina Elizabeth",
              "Cargo": "Director General",
              "size": 5934186,
            },
            {
              "Oficina": "Jardín Maternal Petete",
              "Responsable": "Schiera, Marta",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Jardín Maternal San Vicente",
              "Responsable": "Schneider, Laura",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Jardín Maternal Pimpollitos",
              "Responsable": "Paccioni, Sonia",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Jardín Maternal Pacífico",
              "Responsable": "Ballina, Viviana",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Jadín de Infantes TeVeoBien",
              "Responsable": "Mazza, Ofelia",
              "Cargo": "Responsable",
              "size": 5934186,
            },
            {
              "Oficina": "Jardín Maternal Rayito de Sol",
              "Responsable": "Rodríguez Campins, Paula",
              "Cargo": "Responsable",
              "size": 5934186,
            }
          ]
        },
        {
          "Oficina": "Dirección de Gestión Institucional",
          "Responsable": "-, -",
          "Cargo": "Director General",
          "size": 5934186,
        },
        {
          "Oficina": "Dirección General Coordinación y Monitoreo Interno",
          "Responsable": "Belmonte, Fernando Diego",
          "Cargo": "Director General",
          "size": 5934186,
        },
        {
          "Oficina": "División Jefa de Despacho",
          "Responsable": "...",
          "Cargo": "+++",
          "size": 5934186,
        },
        {
          "Oficina": "Oficina Municipal de Cultos",
          "Responsable": "Mendé, Héctor Gustavo",
          "Cargo": "Responsable",
          "size": 5934186,
        }
      ]
    },
    {
      "Oficina": "Secretaría de Gestión Ambiental",
      "Responsable": "Chanampa, Adriana Elisabet",
      "Cargo": "Secretario",
      "Porcentaje": 12.26,
      "children": [
        {
          "Oficina": "División Jefe de Despacho",
          "Responsable": "...",
          "Cargo": "+++",
          "size": 45287781,
        },
        {
          "Oficina": "Director Coordinador de Gestión Ambiental",
          "Responsable": "Dennis Rozas, Gabriela",
          "Cargo": "Director Coordinador",
          "children": [
            {
              "Oficina": "Departamento de Epidemiología Ambiental",
              "Responsable": "Garay, Vilma Edith",
              "Cargo": "Jefe de Departamento",
              "size": 45287781,
            },
            {
              "Oficina": "Departamento Saneamiento Ambiental",
              "Responsable": "Porcelli, Andrea",
              "Cargo": "Jefe de Departamento",
              "size": 45287781,
            },
            {
              "Oficina": "Departamento Saneamiento Ambiental",
              "Responsable": "González, Hugo Patricio",
              "Cargo": "Jefe de División",
              "size": 45287781,
            },
            {
              "Oficina": "División Habilitaciones Industrias y Nocturnidad",
              "Responsable": "Soberón, Laura Fabiana",
              "Cargo": "Jefe de División",
              "size": 45287781,
            },
            {
              "Oficina": "División Control Limpieza Urbana",
              "Responsable": "Barco, Fabián Ricardo",
              "Cargo": "Jefe de División",
              "size": 45287781,
            },
            {
              "Oficina": "Plan A.P.E.L.L.",
              "Responsable": "Ayala, Daniel",
              "Cargo": "Coordinador",
              "size": 45287781,
            },
            {
              "Oficina": "Comité Técnico Ejecutivo",
              "Responsable": "Pérez, César Horacio",
              "Cargo": "Coordinador",
              "size": 45287781,
            },
          ]
        },
      ]
    },
    {
      "Oficina": "Secretaría de Seguridad y Protección Ciudadana",
      "Responsable": "Álvarez Porte, Emiliano",
      "Cargo": "Secretario",
      "Porcentaje": 6.49,
      "children": [
        {
          "Oficina": "Subsecretaría de Políticas de Seguridad y Participación Ciudadana en Materia de Prevención",
          "Responsable": "Tucat, Federico Gabriel",
          "Cargo": "Subsecretario",
          "children": [
            {
              "Oficina": "División Protección Ciudadana",
              "Responsable": "-, -",
              "Cargo": "Jefe de División",
              "size": 38579124 ,
            }
          ]
        },
        {
          "Oficina": "Dirección General de Defensa Civil",
          "Responsable": "Nadal, Diego",
          "Cargo": "Director General",
          "size": 38579124,
        },
        {
          "Oficina": "Centro Único de Monitoreo",
          "Responsable": "Alonso, Marcelo",
          "Cargo": "Director General",
          "size": 38579124,
        },
        {
          "Oficina": "Departamento de Comunicaciones",
          "Responsable": "Hernández, Raúl",
          "Cargo": "Jefe de Departamento",
          "size": 38579124,
        },
        {
          "Oficina": "División Jefe de Despacho",
          "Responsable": "...",
          "Cargo": "+++",
          "size": 38579124,
        }
      ]
    },
    {
      "Oficina": "Secretaría de Salud",
      "Responsable": "Pastori, Claudio",
      "Cargo": "Secretario",
      "Porcentaje": 20.74,
      "children": [
        {
          "Oficina": "Subsecretaría de Salud",
          "Responsable": "Elliker, Susana María",
          "Cargo": "Subsecretario",
          "children": [
            {
              "Oficina": "Dirección de Infraestructura y Financiamiento",
              "Responsable": "Groppa, Jorge Luis",
              "Cargo": "Director",
              "size": 34234581,
            },
            {
              "Oficina": "Dirección de Geriatría",
              "Responsable": "Iezzi, Eduardo",
              "Cargo": "Director",
              "size": 34234581,
            },
            {
              "Oficina": "Dirección de Programas y Auditoría",
              "Responsable": "Crisafulli, Alejandra Matilde",
              "Cargo": "Director",
              "size": 34234581,
            },
            {
              "Oficina": "Dirección Atención Integral de la Salud",
              "Responsable": "Agüero, Elizabeth Luz",
              "Cargo": "Director",
              "children": [
                {
                  "Oficina": "Área Programática II",
                  "Responsable": "Barbieri, Lorena",
                  "Cargo": "Coordinador",
                  "size": 34234581,
                },
                {
                  "Oficina": "Área Programática III",
                  "Responsable": "Brescia, Sergio",
                  "Cargo": "Coordinador",
                  "size": 34234581,
                },
                {
                  "Oficina": "Área Programática IV",
                  "Responsable": "Herrera, Virginia",
                  "Cargo": "Coordinador",
                  "size": 34234581,
                },
                {
                  "Oficina": "Área Programática V",
                  "Responsable": "Ferraro, Cristina",
                  "Cargo": "Coordinador",
                  "size": 34234581,
                },
                {
                  "Oficina": "Área Programática VI",
                  "Responsable": "Valent, Ana",
                  "Cargo": "Coordinador",
                  "size": 34234581,
                },
                {
                  "Oficina": "Área Programática VII",
                  "Responsable": "Maroun, Carlos",
                  "Cargo": "Coordinador",
                  "size": 34234581,
                },
                {
                  "Oficina": "Área Programática VIII",
                  "Responsable": "Dotta, Claudio",
                  "Cargo": "Coordinador",
                  "size": 34234581,
                },
                {
                  "Oficina": "Área Programática IX",
                  "Responsable": "Zárate, Silvia",
                  "Cargo": "Coordinador",
                  "size": 34234581,
                },
                {
                  "Oficina": "Área Programática XI",
                  "Responsable": "Pérez, Guillermo",
                  "Cargo": "Coordinador",
                  "size": 34234581,
                }
              ]
            },
            {
              "Oficina": "Dirección de Epidemiología, Docencia y Calidad",
              "Responsable": "Deblauwe, Gerardo José",
              "Cargo": "Director",
              "size": 34234581,
            },
            {
              "Oficina": "División Jefe de Despacho",
              "Responsable": "...",
              "Cargo": "+++",
              "size": 34234581,
            }
          ]
        },
        {
          "Oficina": "Dirección de Bromatología",
          "Responsable": "Jouglard, Mario",
          "Cargo": "Director",
          "size": 34234581,
        },
        {
          "Oficina": "Dirección de Veterinaria",
          "Responsable": "San Juan, Fernando Omar",
          "Cargo": "Director",
          "children": [
            {
              "Oficina": "División Jefe de Despacho",
              "Responsable": "...",
              "Cargo": "+++",
              "size": 34234581,
            }
          ]
        },
        {
          "Oficina": "Dirección Zoonosis",
          "Responsable": "Vidal, Pablo Roberto",
          "Cargo": "Director",
          "size": 34234581,
        },
        {
          "Oficina": "División Jefe de Despacho",
          "Responsable": "...",
          "Cargo": "+++",
          "size": 34234581,
        },
      ]
    },
    {
      "Oficina": "Secretaría de Modernización y Calidad de Gestión",
      "Responsable": "Quartucci, Elisa Virginia",
      "Cargo": "Secretario",
      "Porcentaje": 0.58,
      "children": [
        {
          "Oficina": "Dirección General de Modernización",
          "Responsable": "Wirsky, Sabina",
          "Cargo": "Director General",
          "children": [
            {
              "Oficina": "Dirección de Infraestructura Tecnológica",
              "Responsable": "Fernández, Patricia",
              "Cargo": "Subdirector",
              "size": 4297984,
            },
            {
              "Oficina": "0-800-BAHIA",
              "Responsable": "Fierro, Paola Soledad",
              "Cargo": "Responsable",
              "size": 4297984,
            },
          ]
        },
        {
          "Oficina": "Dirección General de Calidad de Gestión",
          "Responsable": "Fernández, José Ignacio Mario",
          "Cargo": "Director General",
          "children": [
            {
              "Oficina": "Departamento Administrativo de Redes",
              "Responsable": "Escobar, Gustavo Fabián",
              "Cargo": "Jefe de Departamento",
              "size": 4297984,
            }
          ]
        },
        {
          "Oficina": "División Jefe de Despacho",
          "Responsable": "...",
          "Cargo": "+++",
          "size": 4297984,
        },
      ]
    }
  ]
};

gastos = [
  {
      "Oficina": "Intendencia",
      "Gasto": 54993207
  },
  {
      "Oficina": "Secretaría Privada",
      "Gasto": 54993207
  },
  {
      "Oficina": "Secretaría de Asesoría Letrada",
      "Gasto": 14240774
  },
  {
      "Oficina": "Secretaría de Innovación Tecnológica y Desarrollo Creativo",
      "Gasto": 8682967
  },
  {
      "Oficina": "Secretaría de Hacienda y Desarrollo Económico",
      "Gasto": 152448574
  },
  {
      "Oficina": "Secretaría de Gobierno",
      "Gasto": 159426861
  },
  {
      "Oficina": "Instituto Cultural Municipal",
      "Gasto": 50412286
  },
  {
      "Oficina": "Secretaría de Infraestructura",
      "Gasto": 652679321
  },
  {
      "Oficina": "Secretaría de Políticas Sociales",
      "Gasto": 277826904
  },
  {
      "Oficina": "Secretaría de Gestión Ambiental",
      "Gasto": 340563100
  },
  {
      "Oficina": "Secretaría de Seguridad y Protección Ciudadana",
      "Gasto": 192895623
  },
  {
      "Oficina": "Secretaría de Salud",
      "Gasto": 616222475
  },
  {
      "Oficina": "Secretaría de Modernización y Calidad de Gestión",
      "Gasto": 17191937
  }
];

jsonColores = {
                  "Secretaría de Asesoría Letrada": [
                    "#721841", "#b32666", "#d32d78", "#da4d8d", "#e06ea2"
                  ],
                  "Instituto Cultural Municipal": [
                    "#9e1a36", "#c02042", "#d12248", "#df3b5e", "#e55c7a"
                  ],
                  "Secretaría de Gestión Ambiental": [
                    "#007c2f", "#00a43d", "#00cb4c", "#00f25b", "#2eff7c"
                  ],
                  "Secretaría de Gobierno": [
                    "#2c7964", "#37967b", "#41b393", "#4abd9d", "#67c8ad"
                  ],
                  "Secretaría de Hacienda y Desarrollo Económico": [
                    "#969d2f", "#b2bb38", "#bdc641", "#c7cf5f", "#d2d87d"
                  ],
                  "Secretaría de Infraestructura": [
                    "#278ba3", "#2ea6c3", "#35b2cf", "#55bed7", "#75cade"
                  ],
                  "Secretaría de Innovación Tecnológica y Desarrollo Creativo": [
                    "#bc0065", "#e3007a", "#ff0b8e", "#ff32a0", "#ff5ab3"
                  ],
                  "Secretaría de Modernización y Calidad de Gestión": [
                    "#c02913", "#e43117", "#e93e26", "#ed5e49", "#f07d6d"
                  ],
                  "Secretaría de Políticas Sociales": [
                    "#9c0e75", "#c01190", "#e414ab", "#eb21b4", "#ee45c0"
                  ],
                  "Secretaría Privada": [
                    "#3d3c3f", "#5a595d", "#6e6c71", "#817f85", "#959398"
                  ],
                  "Secretaría de Salud": [
                    "#b09200", "#d7b200", "#fed300", "#ffde3a", "#ffde3a"
                  ],
                  "Secretaría de Seguridad y Protección Ciudadana": [
                    "#21549e", "#2865be", "#3878d5", "#588edc", "#79a4e3"
                  ]
              };

jsonLogos = {
    "Intendencia": "../img/muni/intendencia.png",
    "Secretaría de Asesoría Letrada": "../img/muni/asesoria-letrada.png",
    "Instituto Cultural Municipal": "../img/muni/cultura.png",
    "Secretaría de Gestión Ambiental": "../img/muni/gestion-ambiental.png",
    "Secretaría de Gobierno": "../img/muni/gobierno.png",
    "Secretaría de Hacienda y Desarrollo Económico":"../img/muni/hacienda.png",
    "Secretaría de Infraestructura": "../img/muni/infraestructura.png",
    "Secretaría de Innovación Tecnológica y Desarrollo Creativo": "../img/muni/innovacion.png",
    "Secretaría de Modernización y Calidad de Gestión":"../img/muni/modernizacion.png",
    "Secretaría de Políticas Sociales": "../img/muni/politicas-sociales.png",
    "Secretaría Privada": "../img/muni/privada.png",
    "Secretaría de Salud": "../img/muni/salud.png",
    "Secretaría de Seguridad y Protección Ciudadana": "../img/muni/seguridad.png",
    "Municipio" : "../img/muni/municipio.png"
};

var width, height;

if( window.innerHeight > 650 )
{
  width = window.innerHeight - 160; height = window.innerHeight - 160;
}
else
  {
    width = 550; height = 550;
  }

var radius = Math.min(width, height) / 2;
var esconder = false;

var radioCirculo = function(){
  if(!esconder){
    return radius *0.8;
  }
    else {
      return radius *0.5;
    }
}

var svg = d3.select("#organigrama").append("svg")
    .attr("width", width)
    .attr("height", height)
    .append("g")
      .attr("id", "container")
      .attr("transform", "translate(" + width / 2 + "," + (height*0.9) / 2 + ")");

var partition = d3.layout.partition()
    .sort(null)
    .size([2 * Math.PI, radius * radius])
    .value(function(d) { return 1; });

var arc = d3.svg.arc()
    .startAngle(function(d) { return d.x; })
    .endAngle(function(d) { return d.x + d.dx; })
    .innerRadius(function(d) { return Math.sqrt(d.y - 10000); })
    .outerRadius(function(d) { return Math.sqrt(d.dy + d.y - 10000); });

// Bounding circle underneath the sunburst, to make it easier to detect
// when the mouse leaves the parent g.
svg.append("circle")
    .attr("r", radioCirculo)
    .style("opacity", 0);

var fillValue = function(d) {
  var result;
  if(d.Oficina == "Intendencia"){
    result = "#3f3f3f";
  }
  else {
    var index = 0;

    while(d.parent.Oficina != "Intendencia")
    {
        d = d.parent;
        index++;
    }

    result = jsonColores[d.Oficina][index];
  }
  return result;
}

var path = svg.datum(organigrama).selectAll("path")
    .data(partition.nodes)
    .enter()
      .append("path")
          //.attr("display", function(d) { return d.depth ? null : "none"; }) // hide inner ring
          .attr("d", arc)
          .style("stroke", "#eee")
          .style("stroke-width", "0.4")
          .style("fill", fillValue)
          .style("fill-rule", "evenodd")
          .style("opacity", 1)
          .each(stash)
          .on("mouseover", mouseover);

var firstLevel = function(d) {
  var opacity;
  if(esconder)
  {
    if(d.depth != 1){
      opacity = "0.05";
    }
    else {
      opacity = "1";
    }
  }
  else {
    opacity = "1";
  }
  return opacity;
}

d3.selectAll("input").on("change", function change() {
    var value = this.value === "count"
        ? function() { esconder = false; svg.selectAll("circle").attr("r", radioCirculo); return 1; }
        : function(d) { esconder = true; svg.selectAll("circle").attr("r", radioCirculo); return d.size; };

    path.data(partition.value(value).nodes)
            .transition()
              .duration(1500)
              .attrTween("d", arcTween)
              .style("opacity",firstLevel);
  });

// Stash the old values for transition.
function stash(d) {
  d.x0 = d.x;
  d.dx0 = d.dx;
}

// Interpolate the arcs in data space.
function arcTween(a) {
  var i = d3.interpolate({x: a.x0, dx: a.dx0}, a);
  return function(t) {
    var b = i(t);
    a.x0 = b.x;
    a.dx0 = b.dx;
    return arc(b);
  };
}

// Add the mouseleave handler to the bounding circle.
d3.select("#container").on("mouseleave", mouseleave);

// Fade all but the current sequence, and show it in the breadcrumb trail.
function mouseover(d) {
  if(!esconder)
  {
    d3.select("#dependencia").text(d.Oficina);
    d3.select("#responsable").text(d.Responsable);
    d3.select("#cargo").text(d.Cargo);

    $("table").css("visibility", "visible");

    var index = d;
    if(d.Oficina != "Intendencia")
        while (index.parent.parent) { index = index.parent; }

    d3.select("#imglogo")
          .attr("src", jsonLogos[index.Oficina]);

    // Fade all the segments.
    d3.selectAll("path").style("opacity", 0.3);
    var sequenceArray = getAncestors(d);
    // Then highlight only those that are an ancestor of the current segment.
    svg.selectAll("path")
        .filter(function(node) { return (sequenceArray.indexOf(node) >= 0); })
        .style("opacity", 1);
  }
  else
  {
    if(d.depth == 1)
    {
      d3.select("#dependencia").text(d.Oficina);
      d3.select("#responsable").text(d.Responsable);
      d3.select("#cargo").text(d.Cargo);

      $("#numero").text(d.Porcentaje+" %");
      $("#quote").text("of the total spent");

      var index = d;
      if(d.Oficina != "Intendencia")
          while (index.parent.parent) { index = index.parent; }

      d3.select("#imglogo")
            .attr("src", jsonLogos[index.Oficina]);

      // Fade all the segments.
      d3.selectAll("path").style("opacity", 0.05);
      var sequenceArray = getAncestors(d);
      // Then highlight only those that are an ancestor of the current segment.
      svg.selectAll("path")
          .filter(function(node) { return (sequenceArray.indexOf(node) >= 0); })
          .style("opacity", 1);
    }
  }
}
// Restore everything to full opacity when moving off the visualization.
function mouseleave(d) {

  d3.select("#imglogo")
        .attr("src", jsonLogos["Municipio"]);

  d3.select("#dependencia").text('-');
  d3.select("#responsable").text('-');
  d3.select("#cargo").text('-');

  $("#numero").text("");
  $("#quote").text("");

  // Deactivate all segments during transition.
  d3.selectAll("path").on("mouseover", null);

  // Transition each segment to full opacity and then reactivate it.
  d3.selectAll("path")
      .transition()
      .duration(1000)
      .style("opacity", firstLevel)
      .each("end", function() {
              d3.select(this).on("mouseover", mouseover);
            });
}

// Given a node in a partition layout, return an array of all of
//its ancestor nodes, highest first.
function getAncestors(node) {
  var path = [];
  var current = node;
  while (current.parent) {
    path.unshift(current);
    current = current.parent;
  }
  if(!esconder)
  {
    path.unshift(current);
  }
  return path;
}
